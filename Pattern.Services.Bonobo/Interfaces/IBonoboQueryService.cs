﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pattern.Services.Bonobo.Interfaces
{
    public interface IBonoboQueryService
    {
        JToken GetCommitsByJiraIdAsJToken(string jiraId);
    }
}
