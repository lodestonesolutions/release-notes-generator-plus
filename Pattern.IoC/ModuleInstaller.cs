﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Pattern.IoC.ServiceLocator;

namespace Pattern.IoC
{
    public class ModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IWindsorContainer>().Instance(container));
            container.Register(Component.For<IServiceLocator>().ImplementedBy<ServiceLocator.ServiceLocator>());
        }
    }
}
