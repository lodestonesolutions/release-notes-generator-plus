﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.SqlCommand;
using Pattern.Repository.NHibernate;
using Pattern.Repository.NHibernate.Common.Interfaces;
using Pattern.Security.Entities.Entities;
using Pattern.Security.Entities.Interfaces;

namespace Pattern.Security.Entities.Repository
{
    public class SecurityRepository<TEntity, TKey> : NHibernateRepository<TEntity, TKey>, 
        ISecurityRepository<TEntity, TKey> where TEntity : class
    {
        public SecurityRepository(ISessionFactory factory): base(factory)
        {
                
        }

        public IList<Activity> GetUserActivities(string username)
        {
            IList<Activity> result;
            Role role = null;
            User user = null;

            using (var uow = BeginUnitOfWork())
            {
                result = uow.Session.QueryOver<Activity>()
                    .JoinAlias(x => x.Roles, () => role, JoinType.InnerJoin)
                    .JoinAlias(x => role.Users, () => user, JoinType.InnerJoin)
                    .Where(activity => activity.IsActive && role.IsActive && user.Username == username && user.IsActive)
                    .Fetch(x => x.Roles).Eager
                    .List();
            }
            
            return result;
        }

        public new virtual IUnitOfWork BeginUnitOfWork()
        {
            return base.BeginUnitOfWork();
        }
    }
}
