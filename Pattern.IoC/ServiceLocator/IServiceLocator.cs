﻿using System.Collections.Generic;

namespace Pattern.IoC.ServiceLocator
{
    public interface IServiceLocator
    {
        TService GetInstance<TService>();
        TService GetInstance<TService>(string key);
        IEnumerable<TService> GetAllInstances<TService>();
    }
}
