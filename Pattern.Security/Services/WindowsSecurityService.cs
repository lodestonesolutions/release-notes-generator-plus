﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Pattern.Security.Entities.Entities;
using Pattern.Security.Entities.Interfaces;
using Pattern.Security.Interfaces;

namespace Pattern.Security.Services
{
    public class WindowsSecurityService : IWindowsSecurityService
    {
        private readonly ISecurityRepository<User, int> _userRepository;
        
        public WindowsSecurityService(ISecurityRepository<User, int>  userRepository)
        {
            _userRepository = userRepository;
        }

        public WindowsIdentity GetUserPricipal()
        {
            return WindowsIdentity.GetCurrent();
        }

        public string GetUserName()
        {
            var identity = GetUserPricipal();
            return identity != null ? identity.Name : null;
        }

        public User GetUser(int userId)
        {
            return _userRepository.Get(x => x.Id == userId && x.IsActive);
        }

        public User GetUser(string username)
        {
            return _userRepository.Get(x => x.Username == username && x.IsActive);
        }

        public IList<Role> GetUserRoles(string username)
        {
            return _userRepository.GetAll(x => x.Username == username && x.IsActive, user => user.Roles)
                .SelectMany(x => x.Roles)
                .ToList();
        }

        public IList<Activity> GetUserActivities(string username)
        {
            var activities = _userRepository.GetUserActivities(username);
            return activities;
        }
    }
}
 