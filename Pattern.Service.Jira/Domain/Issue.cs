﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Pattern.Extensions;
using Pattern.Services.Jira.Constants;

namespace Pattern.Services.Jira.Domain
{
    public class Issue
    {
        /// <summary>
        /// The key of the JIRA issue
        /// </summary>
        public virtual string Key { get; set; }

        /// <summary>
        /// Self referencing HREF
        /// </summary>
        public virtual string Self { get; set; }

        /// <summary>
        /// Dictionary of all fields for the issue
        /// </summary>
        public Dictionary<string, Field> Fields { get; set; }

        /// <summary>
        /// The title or summary of the issue
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// The description or body of the issue
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The status of the issue i.e. open, closed
        /// </summary>
        public virtual Status Status { get; set; }

        /// <summary>
        /// The type of the issue i.e. Request
        /// </summary>
        public IssueType IssueType { get; set; }

        /// <summary>
        /// The project the issue belongs to
        /// </summary>
        public Project Project { get; set; }

        /// <summary>
        /// The priority of the issue
        /// </summary>
        public Priority Priority { get; set; }

        /// <summary>
        /// The created date and time
        /// </summary>
        public DateTime? Created { get; set; }

        /// <summary>
        /// The updated date and time
        /// </summary>
        public virtual DateTime? Updated { get; set; }

        /// <summary>
        /// The user who reported the issue
        /// </summary>
        public JiraUser Reporter { get; set; }

        /// <summary>
        /// The user who the issue is assigned to
        /// </summary>
        public JiraUser Assignee { get; set; }

        /// <summary>
        /// The list of comments for the issue
        /// </summary>
        public virtual IList<Comment> Comments { get; set; }

        /// <summary>
        /// The resolution for the issue i.e. fixed
        /// </summary>
        public Resolution Resolution { get; set; }

        /// <summary>
        /// The resolution date or the issue
        /// </summary>
        public DateTime? ResolutionDate { get; set; }

        public bool HasData { get; set; }

        public void LoadPropertiesFromFields()
        {
            if (Fields == null || Fields.Count == 0)
                return;

            var f1 = GetFieldByName(JiraFields.Summary);
            Summary =  f1?.Value;

            var f2 = GetFieldByName(JiraFields.Description);
            Description = f2?.Value;

            Status = GetFieldObject<Status>(JiraFields.Status);
            IssueType = GetFieldObject<IssueType>(JiraFields.IssueType);
            Priority = GetFieldObject<Priority>(JiraFields.Priority);
            Reporter = GetFieldObject<JiraUser>(JiraFields.Reporter);
            Assignee = GetFieldObject<JiraUser>(JiraFields.Assignee);
            Resolution = GetFieldObject<Resolution>(JiraFields.Resolution);

            var f3 = GetFieldByName(JiraFields.Created);
            Created = f3 != null ? GetJavaDateTime(f3.Value) : new DateTime?();

            var f4 = GetFieldByName(JiraFields.Updated);
            Updated = f4 != null ? GetJavaDateTime(f4.Value) : new DateTime?();

            Comments = GetFieldObject<List<Comment>>(JiraFields.Comment);

            var f5 = GetFieldByName(JiraFields.ResolutionDate);
            ResolutionDate = f5 != null ? GetJavaDateTime(f5.Value) : new DateTime?();

            HasData = true;
        }

        /// <summary>
        /// Gets the Field by name
        /// </summary>
        /// <param name="name">The name of the field</param>
        /// <returns>The Field object</returns>
        public Field GetFieldByName(string name)
        {
            if (Fields == null)
                throw new Exception("Field collection has not been initialised");

            return Fields.ContainsKey(name) ? Fields[name] : null;
        }

        public T GetFieldObject<T>(string name)
        {
            var f = GetFieldByName(name);

            if (f != null && f.Value.IsNotNullOrEmpty())
                return JsonConvert.DeserializeObject<T>(f.Value);

            return default(T);
        }

        protected DateTime? GetJavaDateTime(string javaDate)
        {
            var dt = javaDate.Substring(0, 19);

            var yr = dt.Substring(0, 4).ToInt();
            var mt = dt.Substring(5, 2).ToInt();
            var dy = dt.Substring(8, 2).ToInt(); ;
            var h = dt.Substring(11, 2).ToInt();
            var m = dt.Substring(14, 2).ToInt();
            var s = dt.Substring(17, 2).ToInt();

            return new DateTime(yr, mt, dy, h, m, s);
        }
    }
}
