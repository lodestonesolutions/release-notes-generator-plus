﻿using OpenXmlPowerTools;

namespace ReleaseNotesGenerator
{
    public class DocumentSegment
    {
        public WmlDocument Document { get; set; } 
        public string Id { get; set; }
    }
}