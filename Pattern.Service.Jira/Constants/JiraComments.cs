﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern.Services.Jira.Constants
{
    public static class JiraComments
    {
        public const string PermanentAdjustmentApproved = "Adjustment has been approved, field: {0} to value: {1}";
        public const string PermanentAdjustmentFileApproved = "Adjustment file {0} has been approved.";
        public const string PermanentAdjustmentBatchApproved = "Adjustment batch ID: {0} has been approved.";
        public const string PermanentAdjustmentBatchFailedMultipleJira = "ERROR: Adjustment file: {0} contains more then one Jira ticket. \r\n Operation aborted.";
    }
}
