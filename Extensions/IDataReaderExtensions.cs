﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using Pattern.Entities;

namespace Pattern.Extensions
{
    public static class IDataReaderExtensions
    {
        public static List<T> MapDataToEntityCollection<T>(this IDataReader dr)
        {
            var businessEntityType = typeof(T);
            var entitys = new List<T>();
            var hashtable = new Hashtable();
            var properties = businessEntityType.GetProperties();
            foreach (var info in properties)
            {
                var sVal = GetPropertyMap(info);
                hashtable[sVal] = info;
            }
            while (dr.Read())
            {
                var newObject = (T)Activator.CreateInstance(typeof(T));
                for (var index = 0; index < dr.FieldCount; index++)
                {
                    var info = (PropertyInfo)hashtable[dr.GetName(index).ToUpper()];
                    if ((info == null) || !info.CanWrite)
                    {
                        continue;
                        
                    }
                    var o = (dr.GetValue(index) == DBNull.Value) ? null : dr.GetValue(index);
                    info.SetValue(newObject, o, null);
                }
                entitys.Add(newObject);
            }
            dr.Close();
            return entitys;
        }
        /// <summary>
        /// maps a data reader block to a generic entity collection
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ge">the prototypical entity to be used as conversation (must have all properties that will be mapped from dr)</param>
        /// <returns></returns>
        public static List<BaseGenericEntity> MapDataToGenericEntityCollection(this IDataReader dr, IGenericEntity ge)
        {
            var hashtable = new Hashtable();
            var properties = ge.GetProperties();
            var entities = new List<BaseGenericEntity>();

            foreach (var s in properties)
            {
                hashtable[s] = s;
            }
            var geType = ge.GetType();

            while (dr.Read())
            {
                var newObject = (BaseGenericEntity)Activator.CreateInstance(geType);
                newObject.EntitySource = ge.EntitySource;
                newObject.PrimaryKeyPropertyComponents = new List<string>();
                newObject.PrimaryKeyPropertyComponents.AddRange(ge.PrimaryKeyPropertyComponents);

                for (var index = 0; index < dr.FieldCount; index++)
                {
                    var prop = hashtable[dr.GetName(index).ToUpper()];
                    if (prop == null)
                    {
                        continue;
                    }
                    var o = (dr.GetValue(index) == DBNull.Value) ? null : dr.GetValue(index);
                    newObject.SetProperty(prop.ToString(), o, ge.TryGetPropertyType(prop.ToString()));
                }
                entities.Add(newObject);
            }
            dr.Close();
            return entities;
        }
        /// <summary>
        /// maps a data reader block to a generic entity
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ge">the prototypical entity to be used as conversation (must have all properties that will be mapped from dr)</param>
        /// <returns></returns>
        public static IGenericEntity MapDataToGenericEntity(this IDataReader dr, IGenericEntity ge)
        {
            var hashtable = new Hashtable();
            var properties = ge.GetProperties();
            
            foreach (var s in properties)
            {
                hashtable[s] = s;
            }
            var geType = ge.GetType();
            var newObject = (IGenericEntity)Activator.CreateInstance(geType);

            while (dr.Read())
            {

                for (var index = 0; index < dr.FieldCount; index++)
                {
                    var prop = hashtable[dr.GetName(index).ToUpper()];
                    if (prop == null)
                    {
                        continue;
                    }
                    var o = (dr.GetValue(index) == DBNull.Value) ? null : dr.GetValue(index);
                    newObject.SetProperty(prop.ToString(),o, ge.TryGetPropertyType(prop.ToString()));
                }
                break;
            }
            dr.Close();
            return newObject;
        }
        public static T MapDataToEntity<T>(this IDataReader dr)
        {
            var businessEntityType = typeof(T);
            var hashtable = new Hashtable();
            var properties = businessEntityType.GetProperties();
            foreach (var info in properties)
            {
                var sVal = GetPropertyMap(info);
                hashtable[sVal] = info;
            }

            var newObject = (T)Activator.CreateInstance(typeof(T));

            while (dr.Read())
            {
                
                for (var index = 0; index < dr.FieldCount; index++)
                {
                    var info = (PropertyInfo)hashtable[dr.GetName(index).ToUpper()];
                    if ((info == null) || !info.CanWrite)
                    {
                        continue;

                    }
                    var o = (dr.GetValue(index) == DBNull.Value) ? null : dr.GetValue(index);
                    info.SetValue(newObject, o, null);
                }
                break;
            }
            dr.Close();
            return newObject;
        }
        /// <summary>
        /// Returns the name of the field the given property is to be mapped to using the description attribute, is no such attribute is found then the type name is returned instead.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static string GetPropertyMap(PropertyInfo info)
        {
            if (info.GetCustomAttributes(true).Length <= 0)
            {
                return info.Name.ToUpper();
            }
            foreach (var a in info.GetCustomAttributes(true).OfType<DescriptionAttribute>())
            {
                return (a).Description;
            }
            return info.Name.ToUpper();
        }
        
    }
}
