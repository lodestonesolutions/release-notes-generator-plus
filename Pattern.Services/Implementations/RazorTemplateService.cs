﻿using System;
using System.IO;
using Castle.Core;
using Pattern.Services.Interfaces;
using RazorEngine;

namespace Pattern.Services.Implementations
{
    [CastleComponent(typeof(IRazorTemplateService))]
    public class RazorTemplateService : IRazorTemplateService
    {
        public string ParseTemplate<T>(string templateFilename, T model, string cacheName)
        {
            var template = LoadTemplateFile(templateFilename);
            var result = Razor.Parse(template, model, cacheName);
            return result;
        }

        public string ParseTemplate<T>(string templateFilename, T model)
        {
            var template = LoadTemplateFile(templateFilename);
            var result = Razor.Parse(template, model);
            return result;
        }

        private static string LoadTemplateFile(string templateFilename)
        {
            if (!File.Exists(templateFilename))
                throw new ArgumentException("File at path " + templateFilename + " does not exist");

            string txt;
            using (var sr = new StreamReader(templateFilename))
            {
                txt = sr.ReadToEnd();
                sr.Close();
            }

            return txt;
        }
    }
}
