﻿using System.Collections.Generic;
using Pattern.Repository;
using Pattern.Repository.NHibernate.Common.Interfaces;
using Pattern.Security.Entities.Entities;
using PatternPlayground.Repository;

namespace Pattern.Security.Entities.Interfaces
{
    public interface ISecurityRepository<TEntity, in TKey> : IOrmRepository<TEntity, TKey> where TEntity : class
    {
        IList<Activity> GetUserActivities(string username);

        IUnitOfWork BeginUnitOfWork();
    }
}
