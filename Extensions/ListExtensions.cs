﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pattern.Extensions;
using System.Runtime.CompilerServices;

namespace Extensions
{
    public static class ListExtensions
    {
        public static string GetString<T>(this List<T> s, string deliminator)
        {
            StringBuilder sb = new StringBuilder();
          
           foreach(var item in s)
           {
               sb.Append(item.ToString() + deliminator);
           }

           return sb.ToString().RemoveLastCharacter();
        }

    }
}
