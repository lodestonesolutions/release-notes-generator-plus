﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net;
using Pattern.Security.Interfaces;
using Pattern.Security.Services;

namespace Pattern.Security
{
    public class ModuleInstaller : IWindsorInstaller
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModuleInstaller));
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            #if DEBUG
                container.Register(Component.For<IWindowsSecurityService>().ImplementedBy<MockWindowsSecurityService>());
                _log.Info("Registered MockWindowsSecurityService : IWindowsSecurityService");
            #else
                container.Register(Component.For<IWindowsSecurityService>().ImplementedBy<WindowsSecurityService>());
                _log.Info("Registered WindowsSecurityService : IWindowsSecurityService");
            #endif

        }
    }
}
