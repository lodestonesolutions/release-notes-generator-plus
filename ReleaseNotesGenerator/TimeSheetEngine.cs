﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using Pattern.Services.Jira.Interfaces;

namespace ReleaseNotesGenerator
{
    public class TimeSheetEngine
    {
        private readonly IJiraAccessService _jiraService = Pattern.IoC.Container.Instance.Resolve<IJiraAccessService>();

        public void Run(string filterId)
        {
            var filter = _jiraService.GetFilterAsJToken(filterId);
            var jql = filter["jql"].Value<string>();
            jql = HttpUtility.UrlEncode(jql);
            var filteredTickets = _jiraService.AccessApiAsJToken($"search?jql={jql}");
        }

        private List<JiraTicket> GetJiraTicketsSummary(string jiraId)
        {//Get jiraticket by id
            var ticket = _jiraService.GetIssueAsJToken(jiraId);

            //get linked jira tickets to above ticket
            var issueFields = ticket["fields"];
            var issueLinks = issueFields["issuelinks"];

            return issueLinks.Any() ? issueLinks.Select(GetLinkedIssue).ToList() : new List<JiraTicket> { GetLinkedIssue(ticket) };
        }

        private JiraTicket GetLinkedIssue(JToken issueLink)
        {
            var t = new JiraTicket();
            var oi = issueLink["outwardIssue"] ?? issueLink["inwardIssue"];
            //get jira key
            t.JiraKey = oi?["key"].ToString() ?? issueLink["key"].ToString();
            var ti = _jiraService.GetIssueAsJToken(t.JiraKey);

            //get ticket title
            t.Title = ti["fields"]["summary"].ToString();
            //get ticket description
            t.Description = ti["fields"]["description"].ToString();

            //get ticket attachment
            foreach (var a in ti["fields"]["attachment"])
            {
                t.Attachments.Add(a["content"].ToString());
            }

            t.ExpectedChange = ti["fields"]["customfield_11180"]?.ToString();//expected change custom field;

            //find ticket code in comments
            t.CodeSamples = ti["fields"]["comment"]["comments"].Where(c => c["body"].ToString().Contains("============================="))
                .Aggregate("", (current, c) => current + ("\r\n" + c["body"].ToString()));
            //get affected files - ((Index: )(.)*)+
            var rx = new Regex("((Index: )(.)*)+");
            var matches = rx.Matches(t.CodeSamples);

            t.FilesAffected =
                (from object match in matches select match.ToString().Replace("Index: ", "") + "\r\n")
                .Aggregate("", (current, s) => current + (s + "\r\n"));
            return t;
        }

    }
}
