﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;
using Pattern.Extensions;

namespace Extensions
{
    public static class FormCollectionExtensions
    {
        /// <summary>
        /// Every nested object or collection must be deserialized in json form otherwise this function will fail !
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fc"></param>
        /// <returns></returns>
        public static T ToObject<T>(this FormCollection fc)
        {
            var typeSource = typeof(T);
            var objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in propertyInfo.Where(property => property.CanWrite))
            {//Assign all source property to taget object 's properties
                if (fc[property.Name].IsNullOrEmpty())
                {
                    continue;
                }

                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String))
                {//check whether property type is value type, enum or string type
                    try
                    {
                        if (property.PropertyType.IsEnum)
                        {
                            try
                            {//try to convert to a number and then apply val
                                var n = Convert.ToInt32(fc[property.Name]);
                                property.SetValue(objTarget, n, null);
                            }
                            catch
                            {//incaseof failure maybe enum came in as text value
                                property.SetValue(objTarget, Convert.ChangeType(fc[property.Name], property.PropertyType), null);
                            }                            
                        }
                        else
                        {
                            property.SetValue(objTarget, Convert.ChangeType(fc[property.Name], property.PropertyType), null);
                        }
                        
                    }
                    catch
                    {
                        property.SetValue(objTarget, default(Object), null);
                    }
                    
                }
                else
                {//else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    var objJsonValue = fc[property.Name];

                    if (property.IsPropertyACollection() && !objJsonValue.Trim().StartsWith("["))
                    {
                        objJsonValue = "[" + objJsonValue + "]";
                    }

                    object dserJson;
                    try
                    {//try to deserialize
                        dserJson = JsonConvert.DeserializeObject(objJsonValue, property.PropertyType);
                    }
                    catch
                    {// if failed try to remove $id and try again
                        objJsonValue = objJsonValue.Replace("\"$id\":\"1\",", "");
                        dserJson = JsonConvert.DeserializeObject(objJsonValue, property.PropertyType);
                    }
                    
                    property.SetValue(objTarget, dserJson, null);
                }
            }
            return (T)objTarget;
        }
    }
}
