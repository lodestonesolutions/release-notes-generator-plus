﻿namespace Pattern.Services.Jira.Constants
{
    public static class JiraFields
    {
        public const string Summary = "summary";
        public const string Description = "description";
        public const string Status = "status";
        public const string IssueType = "issuetype";
        public const string Priority = "priority";

        public const string Created = "created";
        public const string Updated = "updated";
        public const string Reporter = "reporter";
        public const string Assignee = "assignee";
        public const string Comment = "comment";

        public const string Resolution = "resolution";
        public const string ResolutionDate = "resolutiondate";
        public const string Fields = "fields";
        public const string LinkedIssues = "linkedissues";
    }
}
