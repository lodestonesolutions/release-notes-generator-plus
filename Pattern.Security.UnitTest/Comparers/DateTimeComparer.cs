﻿using System;
using System.Collections;

namespace Pattern.Security.UnitTest.Comparers
{
    public class DateTimeComparer: IEqualityComparer
    {
        public new bool Equals(object x, object y)
        {
            var x1 = x as DateTime?;
            var y1 = y as DateTime?;

            if (x1 == y1)
                return true;

            if (x == null || y == null)
                return false;

            return x1.Value.Subtract(y1.Value) < new TimeSpan(0, 0, 0, 1) && x1.Value.Subtract(y1.Value) > new TimeSpan(0, 0, 0, -1);
        }

        public int GetHashCode(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
