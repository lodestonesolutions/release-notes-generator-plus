﻿using System;
using System.Collections.Generic;
using System.Net;
using log4net;
using Newtonsoft.Json.Linq;
using Pattern.Services.Interfaces;
using Pattern.Services.Jira.Constants;
using Pattern.Services.Jira.Domain;
using Pattern.Services.Jira.Interfaces;
using Pattern.Services.Jira.Properties;
using Pattern.Services.Jira.ScJiraSoapService;
using RestSharp;
using RestSharp.Authenticators;

namespace Pattern.Services.Jira.Services
{
    public class JiraAccessSoapService : IJiraAccessService
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(JiraAccessSoapService));
        private readonly string _jiraServiceBaseUrl;
        private readonly string _username;
        private readonly string _pw;
        private string _token;
        private readonly JiraSoapServiceService Service;

        public JiraAccessSoapService(IConfigurationService configService)
        {
            _logger.Info("JiraAccessSoapService() - Inititating...");
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            _jiraServiceBaseUrl = Settings.Default.JiraServiceBaseUrlRest;
            //_jiraServiceAuthUrl = configService.GetAppSettingValue(AppSettingsKeys.JiraServiceAuthUrl);

            _username = Settings.Default.JiraUsername;
            _pw = Settings.Default.JiraPw;

            if (string.IsNullOrEmpty(_jiraServiceBaseUrl))
                throw new ArgumentException("Jira Base Service URL cannot be blank or null");

            Service = new JiraSoapServiceService();
            GetLogin();
            _logger.Info("JiraAccessSoapService() - Inititated.");
        }

        private void GetLogin()
        {
            try
            {
                
                if (string.IsNullOrEmpty(_token))
                {
                    _token = Service.login(_username, _pw);
                }
                Service.getUser(_token, _username);
            }
            catch (Exception e)
            {//authentication may have timed out try to reauthenticate
                _logger.Info("GetLogin() - failed, relogging.");
                _token = Service.login(_username, _pw);
            }
            
        }

        public Issue GetIssue(string key)
        {
            try
            {
                GetLogin();
                var client = new RestClient(_jiraServiceBaseUrl)
                {
                    CookieContainer = new CookieContainer(),
                    Authenticator = new HttpBasicAuthenticator(_username, _pw)
                };

                var request = new RestRequest("issue/{id}", Method.GET);
                request.AddUrlSegment("id", key);

                var response = client.Execute<RestIssue>(request);
                var issue = response.Data;
                issue.LoadPropertiesFromFields();

                return issue;
            }
            catch (Exception ex)
            {
                _logger.Fatal("GetIssue() - failed: ", ex);
            }
            return null;
        }

        public JToken GetIssueAsJToken(string key)
        {
            throw new NotImplementedException();
        }
        public JToken GetFilterAsJToken(string filterId)
        {
            throw new NotImplementedException();
        }
        public JToken AccessApiAsJToken(string restQueryString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new Jira issue and returns a new Issue object with the details
        /// </summary>
        /// <param name="summary">Issue Summary</param>
        /// <param name="description">Issue Description</param>
        /// <param name="issueType">Issue Type (provided as id) - i.e: 98 = Request , 99 = General</param>
        /// <param name="key">Project key/name - Project under which to add the new issue</param>
        /// <returns>New Issue object represneting the new created issue</returns>
        public Issue CreateIssue(string summary, string description, string issueType, string assignee, string key)
        {
            try
            {
                var remoteIssue = new RemoteIssue
                {
                    project = key,
                    summary = summary,
                    assignee = assignee,
                    description = description,
                    type = issueType,
                    // These 2 custom fields are mandatory when creating issues, 
                    // currently using default value [SICorp] for Support Provider fields
                    // and default value [Minor / Low] for Business Priority
                    customFieldValues = new RemoteCustomFieldValue[] { 
                                    new RemoteCustomFieldValue { customfieldId = "customfield_10382" , values = new String[] { "10555" } }, // Support Provider - 10555 = SICorp
                                    new RemoteCustomFieldValue { customfieldId = "customfield_10385" , values = new String[] { "10533" } }  // Business Priority - 10533 = Minor
                }
                };
                remoteIssue = Service.createIssue(_token, remoteIssue);
                var newIssue = new Issue
                {
                    Summary = remoteIssue.summary,
                    Description = remoteIssue.description,
                    Key = remoteIssue.key,
                    Created = remoteIssue.created,
                    Updated = remoteIssue.updated,
                };
                return newIssue;
            }
            catch (Exception ex)
            {
                _logger.Fatal("CreateIssue() - failed: ", ex);
            }

            return null;
        }

        public void UpdateIssue(Issue issue, string key)
        {
            if (!IsExists(key))
            {
                //todo: log
                return;
            }
            var lstRfv = new List<RemoteFieldValue>();

            //Issue to RemoteFieldValue
            var f = new RemoteFieldValue
            {
                id = JiraFields.Description, 
                values = new[] {issue.Description}
            };

            lstRfv.Add(f);
            Service.updateIssue(_token, key, lstRfv.ToArray());
        }

        public void UpdateStatus(string status, string key)
        {
            if (!IsExists(key))
            {
                //todo: log
                return;
            }
            //TODO:need to figure the workflows for this one
            throw new NotImplementedException();
        }

        public void AddComment(string comment, string key)
        {
            try
            {
                if (!IsExists(key))
                {
                    //todo: log
                    return;
                }
                var rc = new RemoteComment();
                rc.body = comment;
                Service.addComment(_token, key, rc);
            }
            catch (Exception ex)
            {
                _logger.Fatal("AddComment() - failed: ", ex);
            }
            
        }

        private bool IsExists(string key)
        {
            var issue = this.GetIssue(key);
            return issue.HasData;
        }
    }
}
