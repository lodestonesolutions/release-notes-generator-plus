﻿using System;

namespace Pattern.Services.Interfaces
{
    public interface ICacheService
    {
        /// <summary>
        /// Get a value from cache
        /// </summary>
        /// <param name="key">The cache key</param>
        /// <returns></returns>
        object Get(string key);
        /// <summary>
        /// Generic to get an object from cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);

        void Set(string key, object value, int cacheSeconds);
        void Set<T>(string key, T value, int cacheSeconds);
        void Set<T>(string key, T value, TimeSpan slidingExpiration);

        /// <summary>
        /// Remove an item from the cache
        /// </summary>
        /// <param name="key"></param>
        void Invalidate(string key);
    }
}
