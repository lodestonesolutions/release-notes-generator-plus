﻿namespace Pattern.Services.Jira.Domain
{
    public class JiraUser: AddressableNamedEntity
    {
        public string DisplayName { get; set; }
        public bool Active { get; set; }
    }
}
