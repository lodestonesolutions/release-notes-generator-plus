﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using Pattern.Extensions;
using Pattern.Entities;
using PatternPlayground.Repository;

namespace Pattern.Repository.ADO
{
    public class AdoGenericEntityRepository : IRepository<BaseGenericEntity, string>, IGenericEntityRepository<BaseGenericEntity, string>
    {
        private BaseGenericEntity _prototypeGenericEntity;
        private string _sourceName;
        protected const int PAGER = 1000;
        public IDbConnection Connection { get; set; }

        public BaseGenericEntity PrototypeGenericEntity
        {
            get
            {
                if (_prototypeGenericEntity != null)
                {
                    return _prototypeGenericEntity;
                }

                _prototypeGenericEntity = GetPrototypeEntityFromTable();
                return _prototypeGenericEntity;

            }
            set { _prototypeGenericEntity = value; }
        } //it's properties will be used as the prototype to map all data from DB
        /// <summary>
        /// The table name from which to get the data from.
        /// </summary>
        public string SourceName
        {
            get { return _sourceName; }
            set
            {
                if ((string.IsNullOrEmpty(_sourceName) || _sourceName != value) && !string.IsNullOrEmpty(value))
                {//first init for tableName or new different value -> regenerate the prototype entity by new table
                    _sourceName = value;
                    PrototypeGenericEntity = GetPrototypeEntityFromTable();
                }
                _sourceName = value; //null value do nothing.
            }
        }
        public AdoGenericEntityRepository()
        {
            //create any other provider factory.. for Oracle, MySql, etc...
            //TODO: Generic factory
            var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            Connection = factory.CreateConnection(); // will return the connection object, in this case, SqlConnection ...

            if (Connection == null)
            {
                //TODO: Log
                throw new Exception("AdoGenericEntityRepository factory.CreateConnection() returned null connection.");
            }

            Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DataGovConnection"].ConnectionString;
        }

        public BaseGenericEntity Get(string id)
        {
            var lst = ExecuteQuery("SELECT * From {0} WHERE SID = " + id);

            return (lst != null && lst.Count > 0) ? lst[0] : null;
        }
        /// <summary>
        /// Implementation is very inefficient for large amounts of data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BaseGenericEntity> GetAll()
        {
            var lst = ExecuteQuery("SELECT * From {0}");

            return lst;
        }

        public IEnumerable<IEnumerable<BaseGenericEntity>> GetAllYield()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BaseGenericEntity> GetAll(Expression<Func<BaseGenericEntity, bool>> func, Expression<Func<BaseGenericEntity, object>> fetch = null)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BaseGenericEntity> GetAll(Expression<Func<BaseGenericEntity, bool>> whereFunc, List<Expression<Func<BaseGenericEntity, object>>> fetches)
        {
            throw new NotImplementedException();
        }

        public BaseGenericEntity Create(BaseGenericEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(BaseGenericEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Save(BaseGenericEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(BaseGenericEntity entity)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Executes a query against the DB, automatically replaces {0} with Table name (property SourceName)
        /// </summary>
        /// <param name="sSqlQuery"></param>
        /// <returns></returns>
        protected List<BaseGenericEntity> ExecuteQuery(string sSqlQuery)
        {
            var lst = new List<BaseGenericEntity>();
            ValidateEssentials();
            try
            {
                Connection.Open();

                // create command to execute queries...
                var command = Connection.CreateCommand();
                // create a SqlCommand, OracleCommand etc... depende of the provider factory configured.
                command.CommandText = String.Format(sSqlQuery, SourceName);
                var reader = command.ExecuteReader();
                lst = reader.MapDataToGenericEntityCollection(PrototypeGenericEntity);
            }
            catch
            {
                //TODO: Log
            }
            finally
            {
                Connection.Close();
            }
            return lst;
        }
        /// <summary>
        /// Executes a command against the DB, automatically replaces {0} with Table name (property SourceName)
        /// </summary>
        /// <param name="sSqlQuery"></param>
        /// <returns>True if statement succeeded.</returns>
        protected bool ExecuteCommand(string sSqlQuery)
        {
            ValidateEssentials();
            try
            {
                Connection.Open();

                // create command to execute queries...
                var command = Connection.CreateCommand();
                // create a SqlCommand, OracleCommand etc... depende of the provider factory configured.
                command.CommandText = String.Format(sSqlQuery, SourceName);
                var reader = command.ExecuteNonQuery();

                return (reader > 0);
            }
            catch
            {
                //TODO: Log
            }
            finally
            {
                Connection.Close();
            }
            return false;
        }
        protected void ValidateEssentials()
        {
            if (PrototypeGenericEntity == null)
            {
                throw new Exception("No prototype object was set.");
            }
            if (Connection == null)
            {
                throw new Exception("DB connection is null.");
            }
        }

        private BaseGenericEntity GetPrototypeEntityFromTable()
        {
            var bge = new BaseGenericEntity();
            bge.EntitySource = SourceName;

            try
            {
                Connection.Open();

                var command = Connection.CreateCommand();
                command.CommandText = String.Format("SELECT TOP 1 * FROM {0}", SourceName);
                var reader = command.ExecuteReader(CommandBehavior.SchemaOnly);
                var schema = reader.GetSchemaTable();

                if (schema != null)
                {
                    foreach (DataRow row in schema.Rows)
                    {//For each property of the field...
                        bge.SetProperty(row["ColumnName"].ToString(),null);
                    }

                    foreach (var key in schema.PrimaryKey)
                    {//get primary key components
                        bge.PrimaryKeyPropertyComponents.Add(key.ColumnName);
                    }
                }
            }
            finally
            {
                Connection.Close();
            }

            return bge;
        }

        

        public void Dispose()
        {
            //TODO:Implement
            //throw new NotImplementedException();
        }


        
    }
}
