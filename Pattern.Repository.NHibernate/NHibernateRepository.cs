﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using log4net;
using NHibernate;
using NHibernate.Linq;
using Pattern.Extensions;
using Pattern.Repository.NHibernate.Common.Interfaces;
using Pattern.Repository.NHibernate.Common.UnitOfWork;
using PatternPlayground.Repository;


namespace Pattern.Repository.NHibernate
{
    public abstract class NHibernateRepository<TEntity, TKey>
        : IOrmRepository<TEntity, TKey> where TEntity : class
    {
        private readonly ISessionFactory _factory;
        private IUnitOfWork _uow;
        const int NUMBER_OF_ENTITIES_TO_SAVE_IN_A_BATCH = 900;
        const int NUMBER_OF_ENTITIES_TO_DELETE_IN_A_BATCH = 3;

        protected NHibernateRepository(ISessionFactory factory)
        {
            _factory = factory;
        }
        public TEntity Load(TKey id)
        {
            using (var uow = BeginUnitOfWork())
            {
                return uow.Session.Load<TEntity>(id);
            }
        }
        public TEntity Get(TKey id)
        {
            using (var uow = BeginUnitOfWork())
            {
                var res = uow.Session.CreateQuery("from " + typeof(TEntity).Name + " fetch all properties where sid = " + id).SetCacheable(true).SetCacheRegion("Get").SetMaxResults(1).UniqueResult<TEntity>();
                //var o = uow.Session.Get<TEntity>(id);
                //o.CopyObjectWithSerializer<TEntity>();//will cause nhibernate to fetch all the lazy loaded fields.
                uow.Commit();
                return res;
            }
        }
        public TEntity Get(Expression<Func<TEntity, bool>> whereFunc, Expression<Func<TEntity, object>> fetch = null)
        {
            using (var uow = BeginUnitOfWork())
            {
                var o = fetch == null ? uow.Session.Query<TEntity>().Timeout<TEntity>(1000000).FirstOrDefault(whereFunc)
                    : uow.Session.Query<TEntity>().Where(whereFunc).Fetch(fetch).Timeout<TEntity>(1000000).ToList().FirstOrDefault();
                uow.Commit();
                return o;
            }
        }
        public TEntity Get(Expression<Func<TEntity, bool>> whereFunc, List<Expression<Func<TEntity, object>>> fetches)
        {
            using (var uow = BeginUnitOfWork())
            {
                var res = uow.Session.Query<TEntity>().Where(whereFunc).ToFuture();
                foreach (var fetch in fetches)
                {
                    uow.Session.Query<TEntity>().Where(whereFunc).Fetch(fetch).ToFuture();
                }
                uow.Commit();
                return res.FirstOrDefault();
            }
        }
        public IEnumerable<TEntity> GetAll()
        {
            _uow = BeginUnitOfWork();
            var o = _uow.Session.Query<TEntity>();
            _uow.Commit();
            return o;
        }

        public IEnumerable<IEnumerable<TEntity>> GetAllYield()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, int take)
        {
            Expression<Func<TEntity, object>> fetch = null;
            return GetAll(whereFunc, fetch, take);
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, Expression<Func<TEntity, object>> fetch = null, int take = -1)
        {
            using (var uow = BeginUnitOfWork())
            {
                var lst = fetch == null ? uow.Session.Query<TEntity>().Where(whereFunc) : uow.Session.Query<TEntity>().Where(whereFunc).Fetch(fetch);
                uow.Commit();
                return take > 0 ? lst.Take(take).ToList() : lst.ToList();
            }
        }    
   
        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, List<Expression<Func<TEntity, object>>> fetches, int take = -1)
        {
            using (var uow = BeginUnitOfWork())
            {
                var res = uow.Session.Query<TEntity>().Where(whereFunc).ToFuture();
                foreach (var fetch in fetches)
                {
                    uow.Session.Query<TEntity>().Where(whereFunc).Fetch(fetch).ToFuture();
                }
                uow.Commit();
                return take > 0 ? res.Take(take).ToList() : res.ToList();
            }
        }

        public IEnumerable<TEntity> GetAllPaged(Expression<Func<TEntity, bool>> whereFunc, int pageNum, int pageSize, Expression<Func<TEntity, object>> fetch = null)
        {
            using (var uow = BeginUnitOfWork())
            {
                var lst = fetch == null ? uow.Session.Query<TEntity>().Where(whereFunc) : uow.Session.Query<TEntity>().Where(whereFunc).Fetch(fetch);
                lst = lst.Skip((pageNum - 1)*pageSize).Take(pageSize);
                uow.Commit();
                return lst.ToList();
            }
        }

        public IEnumerable<TEntity> GetAllPaged(Expression<Func<TEntity, bool>> whereFunc, int pageNum, int pageSize, List<Expression<Func<TEntity, object>>> fetches)
        {
            using (var uow = BeginUnitOfWork())
            {
                var res = uow.Session.Query<TEntity>().Where(whereFunc).ToFuture();
                foreach (var fetch in fetches)
                {
                    uow.Session.Query<TEntity>().Where(whereFunc).Fetch(fetch).ToFuture();
                }
                res = res.Skip((pageNum - 1) * pageSize).Take(pageSize);
                uow.Commit();
                return res.ToList();
            }
        }

        public int GetCount(Expression<Func<TEntity, bool>> whereFunc = null)
        {
            using (var uow = BeginUnitOfWork())
            {
                var count = whereFunc == null ? uow.Session.Query<TEntity>().Count() : uow.Session.Query<TEntity>().Count(whereFunc);
                uow.Commit();
                return count;
            }
        }
        public TEntity Create(TEntity entity)
        {
            using (var uow = BeginUnitOfWork())
            {
                uow.Session.SaveOrUpdate(entity);
                uow.Commit();
                return entity;
            }
        }

        public void Update(TEntity entity)
        {
            using (var uow = BeginUnitOfWork())
            {
                uow.Session.SaveOrUpdate(entity);
                uow.Commit();
            }
        }

        public void Save(TEntity entity)
        {
            //_factory.GetCurrentSession().Merge();
            using (var uow = BeginUnitOfWork())
            {
                uow.Session.SaveOrUpdate(entity);
                uow.Commit();
            }
        }

       

        /// <summary>
        /// Save a number of entities toegther ina  batch mode 
        /// need to get the number of enttites to save
        /// </summary>
        /// <param name="entities"></param>
        public void Save(List<TEntity> entities)
        {
           var count = 0;

            var uow = BeginUnitOfWork();

            while (count < entities.Count)
            {
                uow.Session.SaveOrUpdate(entities[count]);
                count++;
                if (count % NUMBER_OF_ENTITIES_TO_SAVE_IN_A_BATCH == 0)
                {
                    uow.Commit();
                    uow.Dispose();
                    uow = BeginUnitOfWork();
                }
                
            }

            ///final commit and dispose
            uow.Commit();
            uow.Dispose();
        }

        public void Delete(TEntity entity)
        {
            using (var uow = BeginUnitOfWork())
            {
                uow.Session.Delete(entity);
                uow.Commit();
            }
        }

        public void Delete(List<TEntity> entities)
        {
            var count = 0;

            var uow = BeginUnitOfWork();

            while (count < entities.Count)
            {
                uow.Session.Delete(entities[count]);
                count++;
                if (count % NUMBER_OF_ENTITIES_TO_DELETE_IN_A_BATCH == 0)
                {
                    uow.Commit();
                    uow.Dispose();
                    uow = BeginUnitOfWork();
                }

            }

            ///final commit and dispose
            uow.Commit();
            uow.Dispose();
        }

        public void Dispose()
        {
            if (_uow != null)
            {
                _uow.Dispose();
            }
        }

        public virtual IUnitOfWork BeginUnitOfWork()
        {
            return new DefaultUnitOfWork(_factory);
        }

        protected virtual void InUnitOfWork(Action<ISession> action, ILog log = null, string errorMessage = null)
        {
            using (var uow = BeginUnitOfWork())
            {
                try
                {
                    action(uow.Session);
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    if (uow != null)
                        uow.Rollback();

                    if (log != null && log.IsErrorEnabled)
                        log.Error(String.IsNullOrEmpty(errorMessage) ? ex.ToString()
                            : String.Concat(errorMessage, "\r\n", ex.ToString()));

                    throw;
                }
            }
        }

        protected virtual T InUnitOfWork<T>(Func<ISession, T> func, ILog log = null, string errorMessage = null)
        {
            T result;

            using (var uow = BeginUnitOfWork())
            {
                try
                {
                    result = func(uow.Session);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    if (uow != null)
                        uow.Rollback();

                    if (log != null && log.IsErrorEnabled)
                        log.Error(String.IsNullOrEmpty(errorMessage) ? ex.ToString()
                            : String.Concat(errorMessage, "\r\n", ex.ToString()));

                    throw;
                }
            }

            return result;
        }



    }
}
