﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Pattern.Services.Jira.Domain;
using Pattern.Services.Jira.Interfaces;

namespace Pattern.Services.Jira.Services
{
    public class MockJiraAccessService : IJiraAccessService
    {
        public List<Issue> Issues { get; set; }

        public MockJiraAccessService()
        {
            Issues = new List<Issue>
            {
                new Issue {HasData = true, Key = "SSA-803", Status = new Status {Name = "Approved"},Comments = new List<Comment>()},
                new Issue {HasData = true, Key = "SSA-804", Status = new Status {Name = "Open"},Comments = new List<Comment>()}
            };
        }

        public Issue GetIssue(string key)
        {
            var issue = Issues.FirstOrDefault(i => i.Key == key) ?? new RestIssue { HasData = true, Key = key, Status = new Status { Name = "Approved" },Comments = new List<Comment>()};
            if (Issues.FirstOrDefault(i => i.Key == key) == null)
            {
                Issues.Add(issue);
            }
            return issue;
        }

        public JToken GetIssueAsJToken(string key)
        {
            return null;
        }

        public JToken GetFilterAsJToken(string filterId)
        {
            throw new NotImplementedException();
        }
        public JToken AccessApiAsJToken(string restQueryString)
        {
            throw new NotImplementedException();
        }
        public Issue CreateIssue(string summary, string description, string issueType, string assignee, string key)
        {
            var oIssue = GetIssue(key);
            if (oIssue != null)
            {
                var issue = new Issue { Summary = summary, Description = description, Key = key, IssueType = new IssueType { Id = issueType } };
                Issues.Add(issue);
                oIssue = issue;
            }
            return oIssue;
        }

        public void UpdateIssue(Issue issue, string key)
        {
            var oIssue = GetIssue(key);
            if (oIssue != null)
            {
                Issues.Remove(oIssue);
                Issues.Add(issue);
            }
        }

        public void UpdateStatus(string status, string key)
        {
            return;
        }

        public void AddComment(string comment, string key)
        {
            var oIssue = GetIssue(key);
            if (oIssue == null)
            {
                return;
            }
            oIssue.Comments.Add(new Comment{Body = comment});
        }
    }
}
