﻿using System.Collections.Generic;
using System.Security.Principal;
using Pattern.Security.Entities.Entities;

namespace Pattern.Security.Interfaces
{
    public interface IWindowsSecurityService
    {
        /// <summary>
        /// Get the windows security principal
        /// </summary>
        /// <returns></returns>
        WindowsIdentity GetUserPricipal();

        /// <summary>
        /// Get the user name
        /// </summary>
        /// <returns></returns>
        string GetUserName();

        /// <summary>
        /// Return a user
        /// </summary>
        /// <param name="userId">The user's Id</param>
        /// <returns></returns>
        User GetUser(int userId);

        /// <summary>
        /// Return a user
        /// </summary>
        /// <param name="username">The username</param>
        /// <returns></returns>
        User GetUser(string username);

        /// <summary>
        /// Get the roles for a user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        IList<Role> GetUserRoles(string username);

        /// <summary>
        /// Get the activities for a user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        IList<Activity> GetUserActivities(string username);
    }
}
