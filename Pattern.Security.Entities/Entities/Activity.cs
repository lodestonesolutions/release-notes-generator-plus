﻿using System;
using System.Collections.Generic;
using Pattern.Entities;

namespace Pattern.Security.Entities.Entities
{
    [Serializable]
    public class Activity : BaseEntity<Activity, int>
    {
        public virtual string ActivityName { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual IList<Role> Roles { get; set; }

        public virtual DateTime CreatedOn { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual string ModifiedBy { get; set; }
    }
}
