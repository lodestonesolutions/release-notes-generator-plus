﻿using System;
using System.Collections.Generic;
using Pattern.Entities;

namespace Pattern.Security.Entities.Entities
{
    [Serializable]
    public class Role : BaseEntity<Role, int>
    {
        public virtual string RoleName { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual IList<Activity> Activities { get; set; }
        public virtual IList<User> Users { get; set; }
        
        public virtual DateTime CreatedOn { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual string ModifiedBy { get; set; }
    }
}
