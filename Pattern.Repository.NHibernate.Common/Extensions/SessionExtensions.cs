﻿using NHibernate;

namespace Pattern.Repository.NHibernate.Common.Extensions
{
    public static class SessionExtensions
    {
        /// <summary>
        /// Get the session id from a session
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetSessionId(this ISession self)
        {
            var sessionId = self.GetType().GetProperty("SessionId").GetValue(self, null);
            return sessionId.ToString();
        }
    }
}
