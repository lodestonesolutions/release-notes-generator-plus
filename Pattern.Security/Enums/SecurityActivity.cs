﻿using System.ComponentModel;

namespace Pattern.Security.Enums
{
    public enum SecurityActivity
    {
        [Description("Approve")]
        Approve = 1,

        [Description("CreateNewUploadDelete")]
        CreateNewUploadDelete = 2,

        [Description("EditTransaction")]
        EditTransaction = 3,

        [Description("Execute")]
        Execute = 4,

        [Description("Read")]
        Read = 5,

        [Description("ConfigMetadata")]
        ConfigMetadata = 6,

        [Description("UserManagement")]
        UserManagement = 7
    }
}
