﻿using System;

namespace Pattern.Services.Jira.Domain
{
    public class Comment: AddressableNamedEntity
    {
        public string Body { get; set; }

        public JiraUser Author { get; set; }
        public JiraUser UpdateAuthor { get; set; }

        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
    }
}
