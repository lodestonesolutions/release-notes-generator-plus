﻿using System;
using System.Linq;
using System.Linq.Expressions;
using log4net;
using NHibernate;

namespace Pattern.Repository.NHibernate.Common.Interfaces
{
    public interface IDatabase
    {        
        /// <summary>
        /// Returns the session id
        /// </summary>
        string SessionId { get; }
        /// <summary>
        /// Returns the current session
        /// </summary>
        /// <returns></returns>
        ISession Session { get; }
        /// <summary>
        /// Gets the Session Factory
        /// </summary>
        ISessionFactory SessionFactory { get; }
        /// <summary>
        /// Begin a new unit of work
        /// </summary>
        /// <returns></returns>
        IUnitOfWork BeginUnitOfWork();
        /// <summary>
        /// Will set up, handle rollback and shutdown for a unit of work (invokes the procedure passed to action)
        /// </summary>
        /// <param name="action"></param>
        /// <param name="log">Optional logger used to log errorMessage and any exceptions on Error level</param>
        /// <param name="errorMessage"></param>
        void InUnitOfWork(Action<ISession> action, ILog log = null, string errorMessage = null);
        /// <summary>
        /// Will set up, handle rollback and shutdown for a unit of work (invokes the method passed to action)
        /// </summary>
        /// <param name="func"></param>
        /// <param name="log">Optional logger used to log errorMessage and any exceptions on Error level</param>
        /// <param name="errorMessage"></param>
        T InUnitOfWork<T>(Func<ISession, T> func, ILog log = null, string errorMessage = null);

        void Save<T>(T obj);

        void SaveOrUpdate<T>(T obj);

        IQueryable<T> Query<T>() where T : class;

        IQueryOver<T, T> QueryOver<T>() where T : class;
        IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias) where T : class;
        IQueryOver<T, T> QueryOver<T>(string entityName) where T : class;
        IQueryOver<T, T> QueryOver<T>(string entityName, Expression<Func<T>> alias) where T : class;
    }
}
