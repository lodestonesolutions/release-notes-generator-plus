﻿namespace Pattern.Security.Entities.Constants
{
    public static class TableNames
    {
        public const string UserTable = "dbo.T_USER";
        public const string RoleTable = "dbo.T_ROLE";
        public const string ActivityTable = "dbo.T_ACTIVITY";
    }
}
