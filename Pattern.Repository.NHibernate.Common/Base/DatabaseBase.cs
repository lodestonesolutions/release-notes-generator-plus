﻿using System;
using System.Linq;
using System.Linq.Expressions;
using log4net;
using NHibernate;
using NHibernate.Linq;
using Pattern.Repository.NHibernate.Common.Extensions;
using Pattern.Repository.NHibernate.Common.Interfaces;
using Pattern.Repository.NHibernate.Common.UnitOfWork;

namespace Pattern.Repository.NHibernate.Common.Base
{
    public abstract class DatabaseBase : IDatabase
    {
        public virtual ISessionFactory SessionFactory { get; private set; }

        protected DatabaseBase(ISessionFactory sessionFactory)
        {
            SessionFactory = sessionFactory;
        }

        public virtual ISession Session
        {
            get { return SessionFactory.GetCurrentSession(); }
        }

        public virtual string SessionId
        {
            get { return Session.GetSessionId(); }
        }

        public virtual IUnitOfWork BeginUnitOfWork()
        {
            return new DefaultUnitOfWork(SessionFactory);
        }

        public virtual void InUnitOfWork(Action<ISession> action, ILog log = null, string errorMessage = null)
        {
            using (var uow = BeginUnitOfWork())
            {
                try
                {
                    action(uow.Session);
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    if (uow != null)
                        uow.Rollback();

                    if (log != null && log.IsErrorEnabled)
                        log.Error(String.IsNullOrEmpty(errorMessage) ? ex.ToString()
                            : String.Concat(errorMessage, "\r\n", ex.ToString()));

                    throw;
                }
            }
        }

        public virtual T InUnitOfWork<T>(Func<ISession, T> func, ILog log = null, string errorMessage = null)
        {
            T result;

            using (var uow = BeginUnitOfWork())
            {
                try
                {
                    result = func(uow.Session);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    if (uow != null)
                        uow.Rollback();

                    if (log != null && log.IsErrorEnabled)
                        log.Error(String.IsNullOrEmpty(errorMessage) ? ex.ToString()
                            : String.Concat(errorMessage, "\r\n", ex.ToString()));

                    throw;
                }
            }

            return result;
        }

        public void Save<T>(T obj)
        {
            Session.Save(obj);
        }

        public void SaveOrUpdate<T>(T obj)
        {
            Session.SaveOrUpdate(obj);
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return Session.Query<T>();
        }

        public IQueryOver<T, T> QueryOver<T>() where T : class
        {
            return Session.QueryOver<T>();
        }

        public IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias) where T : class
        {
            return Session.QueryOver(alias);
        }

        public IQueryOver<T, T> QueryOver<T>(string entityName) where T : class
        {
            return Session.QueryOver<T>(entityName);
        }

        public IQueryOver<T, T> QueryOver<T>(string entityName, Expression<Func<T>> alias) where T : class
        {
            return Session.QueryOver(entityName, alias);
        }
    }
}
