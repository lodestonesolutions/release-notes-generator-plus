﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Lifestyle;
using DataGovernance.DataComparison.Entities.Entities;
using DataGovernance.DataComparison.Entities.Enums;
using DataGovernance.DataModification.Repository;
using DataGovernance.Entities.Entities;
using log4net;
using Pattern.Entities;
using Pattern.Extensions;
using Pattern.IoC;
using Pattern.Repository;
using PatternPlayground.Repository;

namespace DataGovernance.DataComparison.BL
{
    public class CompareEngine
    {
        public ulong MapReduceRunCount = 0;
        public ConcurrentBag<string> RunLog { get; set; }
        public ConcurrentBag<Tuple<string, string>> InfSupSets { get; set; }
        IReadOnlyRepository<BaseGenericEntity, string> Repository1 { get; set; }
        IReadOnlyRepository<BaseGenericEntity, string> Repository2 { get; set; }
        public List<string> KeysMap1 { get; set; }
        public List<string> KeysMap2 { get; set; }
        public ComparisonConfig ComparisonConfig { get; set; }
        public string DetailCompareSumTable { get; set; }
        public string CompareDiscTable { get; set; }
        public string ControlTable { get; set; }
        public int ComparisonId { get; set; }
        private static readonly ILog Log = LogManager.GetLogger(typeof(CompareEngine));
        private bool IsAborted = false;

        public CompareEngine(IReadOnlyRepository<BaseGenericEntity, string> repository1, IReadOnlyRepository<BaseGenericEntity, string> repository2, ComparisonConfig conf)
        {
            Repository1 = repository1;
            Repository2 = repository2;
            ComparisonConfig = conf;

            RunLog = new ConcurrentBag<string>();
            InfSupSets = new ConcurrentBag<Tuple<string, string>>();
            KeysMap1 = new List<string>();
            KeysMap2 = new List<string>();
            CompareDiscTable = "TMFDIM.F_GOV_DIFF_DISC";
            ControlTable = "TMFDIM.F_GOV_CONTROL";

            ConfigComparion();
        }

        private void ConfigComparion()
        {
            Repository1.Connection.ConnectionString = ClearProvider(ComparisonConfig.Source1);
            Repository1.SelectFromSource = ComparisonConfig.Source1Query;//"TMFENT.E_CLAIM";
            if (Repository1.PrototypeGenericEntity.PrimaryKeyPropertyComponents.Count < 1)
            {
                Repository1.PrototypeGenericEntity.PrimaryKeyPropertyComponents.AddRange(ComparisonConfig.Source1BkDefinition.Split("||".ToCharArray()));//"CLAIM_BK"
            }

            Repository2.Connection.ConnectionString = ClearProvider(ComparisonConfig.Source2);
            Repository2.SelectFromSource = ComparisonConfig.Source2Query;//"TMFENT.E_CLAIM";
            if (Repository2.PrototypeGenericEntity.PrimaryKeyPropertyComponents.Count < 1)
            {
                Repository2.PrototypeGenericEntity.PrimaryKeyPropertyComponents.AddRange(ComparisonConfig.Source2BkDefinition.Split("||".ToCharArray()));//"CLAIM_BK"
            }

            foreach (var comparisonColumn in ComparisonConfig.SourceMapping)
            {
                KeysMap1.Add(comparisonColumn.Column1);
                KeysMap2.Add(comparisonColumn.Column2);
            }
        }

        /// <summary>
        /// this function is optimized to use the advantages of oracle and assumes the result sets are coming back ordered.
        /// if the result set is not ordered by the PK this comparison will NOT work.
        /// </summary>
        public void DoWork()
        {
            Console.WriteLine("DoWork() - Initiating comparison...");

            GenerateControlHeader();
            try
            {
                RunCompareSquence();
                if (!IsAborted)
                {
                    UpdateControlHeaderStatus("Completed");
                }
                
            }
            catch (Exception)
            {
                UpdateControlHeaderStatus("Failed");
                using (var crRepo = Container.Instance.Resolve<IOrmRepository<ComparisonRun, int>>())
                {
                    var cr = crRepo.Get(ComparisonId);
                    cr.Status = ComparisonRunStatus.Error;
                    crRepo.Save(cr);
                }
                //throw;
            }
            

            //map/reduce MappedData            
            //Repository1.GetAllYield().AsParallel().ForAll(CompareBatch);

            //find in second source everything between sup and inf values -> values that dont exist in source 1.
            //GetAllNoneCompared();
            Console.WriteLine("DoWork() - Comparison complete.");
            //Console.WriteLine("DoWork() - Starting DB result batch insert operations.");

            //do inserts to db

            Console.WriteLine("DoWork() - DONE.");
        }
        private void RunCompareSquence()
        {
            GenerateDiff();
            GenerateResults();

            if (IsAborted)
            {
                UpdateControlHeaderStatus("Aborted");
                return;
            }

            GenerateSummary();
        }
        private void GenerateResults()
        {
            var diffCount = 0;
            Console.WriteLine("GenerateResults() - has begun.");
            var sw = new Stopwatch();
            sw.Start();
            /*
             * DECLARE
                CURSOR c1 IS
                 select s1.claim_bk,S2.CLAIM_BK, s1.claim_number,s2.agency from tmfdim.f_gov_diff_disc gd
                    left join TMFENT.E_CLAIM s1 on s1.claim_bk = gd.Bk 
                    left join TMFENT.E_CLAIM s2 on s2.claim_bk = gd.Bk 
                where s1.claim_number != s2.agency;
                BK1 NVARCHAR2(150);
                BK2 NVARCHAR2(150);
                V1 NVARCHAR2(8000);
                V2 NVARCHAR2(8000);
                BEGIN
                OPEN c1;
                LOOP
                  FETCH c1 INTO BK1,BK2,V1,V2;
                  EXIT WHEN c1%NOTFOUND;
      
                  INSERT INTO TMFDIM.F_GOV_DIFF (Line_of_Business, Claims_Manager,COMPARISON_ID, ADT_DATE, Table_1_Name, Table_2_Name, TABLE_1_BATCHID,
                            TABLE_2_BATCHID,Entity_1_BK, Entity_2_BK, Field,Entity_1_Val, Entity_2_Val)
                            VALUES ('No LOB', 'No CM', 1,SYSDATE,'TMFENT.E_CLAIM', 'TMFENT.E_CLAIM', 0, 
                            0,'claim_number;agency', BK1, BK2, V1, V2);    
                END LOOP;
                CLOSE c1;
                END;
             */
            using (Container.Instance.BeginScope())
            using (var genRepository = Container.Instance.Resolve<IRealignmentRepository<BaseGenericEntity, string>>())
            {
                genRepository.SourceName = CompareDiscTable;
                for (int index = 0; index < ComparisonConfig.SourceMapping.Count; index++)
                {
                    var q = "";
                    try
                    {
                        var comparisonColumn = ComparisonConfig.SourceMapping[index];
                        var cols = GetColumn(comparisonColumn, index, "s1.", "s2.", true);

                        q += "DECLARE " +
                            "CURSOR c1 IS select * from (" +
                             "select DISTINCT " + GetAliasBk("s1", ComparisonConfig.Source1BkDefinition) + "," + GetAliasBk("s2", ComparisonConfig.Source2BkDefinition) + " "
                                + cols.Item1 + " " + cols.Item2 + GetLobAndCmColumns() + ", " + GetAliasBk(" s" + ComparisonConfig.MetaDataTable, ComparisonConfig.ClaimField) + " AS CLAIM_FIELD  from " + CompareDiscTable + " gd " +
                                "left join " + Repository1.FromSource + " s1 on " + GetAliasBk("s1", ComparisonConfig.Source1BkDefinition) + " = gd.Bk " +
                                "left join " + Repository2.FromSource + " s2 on " + GetAliasBk("s2", ComparisonConfig.Source2BkDefinition) + " = gd.Bk ) r " +
                            "where r.s1col" + index + " != r.s2col" + index + " " +
                            "or ((r.s1col" + index + " is null and r.s2col" + index + " is not null) or (r.s1col" + index + " is not null and r.s2col" + index + " is null) ); " +
                            "BK1 NVARCHAR2(150); " +
                            "BK2 NVARCHAR2(150); " +
                            "V1 NVARCHAR2(2000); " +
                            "V2 NVARCHAR2(2000); " +
                            "LOB NVARCHAR2(150); " +
                            "CM NVARCHAR2(150); " +
                            "CF NVARCHAR2(150); " +
                            "BEGIN " +
                            "OPEN c1; " +
                            "LOOP " +
                            "FETCH c1 INTO BK1,BK2,V1,V2,"
                                + (!ComparisonConfig.IsLineOfBusinessStatic ? "LOB," : "") + ""
                                + (!ComparisonConfig.IsClaimsManagerStatic ? "CM," : "") + "CF; " +
                              "EXIT WHEN c1%NOTFOUND; " +
                              "INSERT INTO TMFDIM.F_GOV_DIFF (Line_of_Business, Claims_Manager,COMPARISON_ID, ADT_DATE, Table_1_Name, Table_2_Name, TABLE_1_BATCHID, " +
                                        "TABLE_2_BATCHID,Field,Field2,Entity_1_BK, Entity_2_BK, Entity_1_Val, Entity_2_Val,CLAIM_NUMBER) " +
                                        "VALUES (" + (!ComparisonConfig.IsLineOfBusinessStatic ? "LOB" : "'" + ComparisonConfig.LineOfBusiness) + "', " + (!ComparisonConfig.IsClaimsManagerStatic ? "CM" : "'" + ComparisonConfig.ClaimsManager) +
                                        "', " + ComparisonId + ",SYSDATE,'" + Repository1.FromSource + "', '" + Repository2.FromSource + "', 0, " +
                                        "0,'" + comparisonColumn.Column1 + "','" + comparisonColumn.Column2 + "', BK1, BK2, V1, V2,CF); " +
                            "END LOOP; " +
                            "CLOSE c1; " +
                            "END; ";
                        genRepository.ExecuteCommand(q);

                        var qCount = "select count(*) from TMFDIM.F_GOV_DIFF WHERE COMPARISON_ID = '" + ComparisonId + "' AND Field = '" + comparisonColumn.Column1 + "'";
                        var dc = genRepository.ExecuteScalar(qCount);
                        diffCount += int.Parse(dc);

                        Console.WriteLine("GenerateResults() - column compare " + index + " finished after " + sw.ElapsedMilliseconds);

                        if (diffCount >= 1000000)
                        {
                            IsAborted = true;
                            Console.WriteLine("GenerateResults() - Comparison aborted, too many differences. After: " + sw.ElapsedMilliseconds);
                            Log.Error("GenerateResults() - Comparison aborted, too many differences. After: " + sw.ElapsedMilliseconds);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("GenerateResults() failed, for itteration: " + index + " for query: \r\n" + q, ex);
                        Console.WriteLine("GenerateResults() - column compare " + index + " failed after " + sw.ElapsedMilliseconds);
                    }                   
                    
                }
            }
            Console.WriteLine("GenerateResults() - has finished in " + sw.ElapsedMilliseconds);
        }
        private string GetLobAndCmColumns()
        {
            var s = "";
            if (!ComparisonConfig.IsLineOfBusinessStatic)
            {
                s += ", s" + ComparisonConfig.MetaDataTable + "." + ComparisonConfig.LineOfBusiness;
            }
            if (!ComparisonConfig.IsClaimsManagerStatic)
            {
                s += ", s" + ComparisonConfig.MetaDataTable + "." + ComparisonConfig.ClaimsManager;
            }
            return s;
        }
        private string GetAliasBk(string prefix,string source)
        {//generates for prefix s1 -> REGEXP_REPLACE(s1.CLAIM_NO||s1.SEQUENCE_NO||s1.PAYMENT_TYPE_CODE||s1.UNIQUE_ID,'([[:space:]])')
            if (source.Contains("'-'||") || source.Contains("'-' ||"))
            {
                return OracleRemoveSpaces(prefix + "." + source.Replace("'-'||", "'-'||" + prefix + ".").Replace("'-' ||", "'-'||" + prefix + "."));
            }
            return OracleRemoveSpaces(prefix + "." + source.Replace("||", "||" + prefix + "."));
        }
        private void GenerateControlHeader()
        {
            using (Container.Instance.BeginScope())
            using (var genRepository = Container.Instance.Resolve<IRealignmentRepository<BaseGenericEntity, string>>())
            using (var crRepo = Container.Instance.Resolve<IOrmRepository<ComparisonRun, int>>())
            {
                genRepository.SourceName = ControlTable;
                var tag = crRepo.Get(ComparisonId).Tag;
                var ge = new BaseGenericEntity();
                ge.SetProperty("ID", ComparisonId.ToString());
                ge.SetProperty("COMPARISONNAME", ComparisonConfig.Name);
                ge.SetProperty("COMPARISONTAG", string.IsNullOrEmpty(tag) ? ComparisonConfig.Name + ComparisonId : tag);
                ge.SetProperty("RUNDATE", DateTime.Now.Date.ToString("dd/MMM/yyyy"));
                ge.SetProperty("STATUS", "Running");
                ge.SetProperty("RUNBY", "");
                ge.SetProperty("UPDATEBY", "");
                ge.SetProperty("UPDATEDATETIME", "");
                ge.SetProperty("CONFIGURATION_ID", ComparisonConfig.Id.ToString());
                genRepository.Save(ge);
            }
        }

        private void UpdateControlHeaderStatus(string status)
        {
            using (Container.Instance.BeginScope())
            using (var genRepository = Container.Instance.Resolve<IRealignmentRepository<BaseGenericEntity, string>>())
            {
                genRepository.SourceName = ControlTable;
                genRepository.PrototypeGenericEntity.PrimaryKeyPropertyComponents.Add("ID");
                var ge = genRepository.Get(ComparisonId.ToString());
                ge.SetProperty("STATUS", status);
                genRepository.Save(ge);
            }
        }

        private void GenerateSummary()
        {
            /*
             * select field,count(*) from TMFDIM.F_GOV_DIFF 
                where comparison_id = 0
                group by field
             */
            Console.WriteLine("GenerateSummary() - has begun.");
            var sw = new Stopwatch();
            sw.Start();
            var q = "DECLARE " +
                       "CURSOR c1 IS select field,count(*) as numOfChanges from TMFDIM.F_GOV_DIFF " +
                        "where comparison_id = " + ComparisonId + " " +
                        "group by field ; " +
                        "Field1 NVARCHAR2(150); " +
                        "Field2 NVARCHAR2(150); " +
                        "LOB NVARCHAR2(150); " +
                        "CM NVARCHAR2(150); " +
                        "noc NUMBER; " +
                    "BEGIN " +
                       "OPEN c1; " +
                       "LOOP " +
                          "FETCH c1 INTO Field1, noc; " +
                          "EXIT WHEN c1%NOTFOUND;  " +
                          "INSERT INTO TMFDIM.F_GOV_DIFF_SUM (Line_of_Business, Claims_Manager,COMPARISON_ID, ADT_DATE, Table_1_Name, Table_2_Name, TABLE_1_BATCHID, " +
                                    "TABLE_2_BATCHID,Field,COUNT_OF_DIFFERENCES, DIFFERENCE_TYPE) " +
                                    "VALUES (LOB, CM, " + ComparisonId + ",SYSDATE,'" + Repository1.FromSource + "', '" + Repository2.FromSource + "', 0, " +
                                    "0, Field1, noc,'string'); " +
                       "END LOOP; " +
                       "CLOSE c1; " +
                    "END; ";
            try
            {
                using (Container.Instance.BeginScope())
                using (var genRepository = Container.Instance.Resolve<IRealignmentRepository<BaseGenericEntity, string>>())
                {
                    genRepository.SourceName = CompareDiscTable;
                    genRepository.ExecuteCommand(q);
                }
                Console.WriteLine("GenerateSummary() - has finished in " + sw.ElapsedMilliseconds);
            }
            catch (Exception ex)
            {
                Log.Fatal("GenerateSummary() failed with query: \r\n" + q, ex);
                Console.WriteLine("GenerateSummary() - has failed after " + sw.ElapsedMilliseconds);
            }
            
            
        }
        private void GenerateDiff()
        {
            Console.WriteLine("GenerateDiff() - has begun.");
            var sw = new Stopwatch();
            sw.Start();
            /*
             * DECLARE
                   CURSOR c1 IS select r.CLAIM_BK from
                    ((select * from TMFENT.E_CLAIM minus select * from TMFENT.E_CLAIM) -- all rows that are in T1 but not in T2
                    union all
                    (select * from TMFENT.E_CLAIM minus select * from TMFENT.E_CLAIM)) r;  -- all rows that are in T2 but not in T1
                    BK1 NVARCHAR2(150);
                BEGIN
                   OPEN c1;
                   LOOP
                      FETCH c1 INTO BK1;
                      EXIT WHEN c1%NOTFOUND;     
                      INSERT INTO TMFDIM.F_GOV_DIFF_DISC (BK) VALUES (BK1);    
                   END LOOP;
                   CLOSE c1;
                END; 
             */
            var columns = GetColumns();
            var qDelete = "DELETE FROM " + CompareDiscTable + " ";
            var q = "DECLARE " +
                       "CURSOR c1 IS select DISTINCT r.PKey from " +
                        "((select " + columns.Item1 + " from " + Repository1.FromSource + GetFilter(ComparisonConfig.Source1Filter) + " minus select " + columns.Item2 + " from " + Repository2.FromSource + GetFilter(ComparisonConfig.Source2Filter) + ") " +
                        "union all " +
                        "(select " + columns.Item2 + " from " + Repository2.FromSource + GetFilter(ComparisonConfig.Source2Filter) + " minus select " + columns.Item1 + " from " + Repository1.FromSource + GetFilter(ComparisonConfig.Source1Filter) + ")) r; " +
                        "BK1 NVARCHAR2(500); " +
                    "BEGIN " +
                       "OPEN c1; " +
                       "LOOP " +
                          "FETCH c1 INTO BK1; " +
                          "EXIT WHEN c1%NOTFOUND;  " +
                          "INSERT INTO " + CompareDiscTable + " (BK) VALUES (BK1); " +
                       "END LOOP; " +
                       "CLOSE c1; " +
                    "END; ";

            try
            {
                //execute query
                using (Container.Instance.BeginScope())
                using (var genRepository = Container.Instance.Resolve<IRealignmentRepository<BaseGenericEntity, string>>())
                {
                    genRepository.SourceName = CompareDiscTable;
                    genRepository.ExecuteCommand(qDelete);
                    genRepository.ExecuteCommand(q);
                }
                Console.WriteLine("GenerateDiff() - has finished in " + sw.ElapsedMilliseconds);
            }
            catch (Exception ex)
            {
                Log.Fatal("GenerateDiff() failed with query: \r\n" + q, ex);
                Console.WriteLine("GenerateDiff() - has failed after " + sw.ElapsedMilliseconds);
                throw;
            }
            
        }

        private string GetFilter(string sFilter)
        {
            if (string.IsNullOrEmpty(sFilter))
            {
                return string.Empty;
            }

            return " WHERE " + sFilter;
        }
        private Tuple<string, string> GetColumns()
        {
            var s1 = "";
            var s2 = "";
            for (int index = 0; index < ComparisonConfig.SourceMapping.Count; index++)
            {
                var comparisonColumn = ComparisonConfig.SourceMapping[index];
                var r = GetColumn(comparisonColumn, index);
                s1 += r.Item1;
                s2 += r.Item2;

            }
            s1 = OracleRemoveSpaces("TO_CHAR(" + ComparisonConfig.Source1BkDefinition + ")") + " AS PKey " + s1;
            s2 = OracleRemoveSpaces("TO_CHAR(" + ComparisonConfig.Source2BkDefinition + ")") + " AS PKey " + s2;

            return new Tuple<string, string>(s1, s2);
        }
        /// <summary>
        /// this function will determine the type of the column we're comparing and will apply oracle conversion to it 
        /// </summary>
        /// <param name="cc">the ComparisonColumn object</param>
        /// <param name="index">it's index</param>
        /// <param name="colPrefix1">a prefix to act as an alias incase the column is a part of a named table i.e select b.Id from blah b -> prefix is b</param>
        /// <param name="colPrefix2">a prefix to act as an alias incase the column is a part of a named table i.e select b.Id from blah b -> prefix is b</param>
        /// <returns></returns>
        private Tuple<string, string> GetColumn(ComparisonColumn cc, int index, string colPrefix1 = "",string colPrefix2 = "", bool isClearSpace = false)
        {
            var s1 = "";
            var s2 = "";
            switch (cc.Type)
            {
                case ComparisonType.Date://yyyymmdd
                    var cf1 = cc.Column1Format.IsNotNullOrEmpty() ? ",'" + cc.Column1Format + "'" : "";
                    var cf2 = cc.Column2Format.IsNotNullOrEmpty() ? ",'" + cc.Column2Format + "'" : "";
                    if (isClearSpace)
                    {
                        s1 += ", " + OracleRemoveSpaces("TO_CHAR(tmfdim.to_date_exception(" + colPrefix1 + "\"" + cc.Column1 + "\"" + cf1 + "),'DDMMYY')") + " AS " + colPrefix1.Replace(".", "") + "col" + index;
                        s2 += ", " + OracleRemoveSpaces("TO_CHAR(tmfdim.to_date_exception(" + colPrefix2 + "\"" + cc.Column2 + "\"" + cf2 + "),'DDMMYY')") + " AS " + colPrefix2.Replace(".", "") + "col" + index;
                    }
                    else
                    {
                        s1 += ", TO_CHAR(tmfdim.to_date_exception(" + colPrefix1 + "\"" + cc.Column1 + "\"" + cf1 + "),'DDMMYY') AS " + colPrefix1.Replace(".", "") + "col" + index;
                        s2 += ", TO_CHAR(tmfdim.to_date_exception(" + colPrefix2 + "\"" + cc.Column2 + "\"" + cf2 + "),'DDMMYY') AS " + colPrefix2.Replace(".", "") + "col" + index;
                    }
                    
                    break;
                case ComparisonType.Number:
                    if (isClearSpace)
                    {
                        s1 += ", CAST(" + OracleRemoveSpaces("TO_CHAR(" + colPrefix1 + "\"" + cc.Column1 + "\"" + ")") + " AS Number) AS " + colPrefix1.Replace(".", "") + "col" + index;
                        s2 += ", CAST(" + OracleRemoveSpaces("TO_CHAR(" + colPrefix2 + "\"" + cc.Column2 + "\"" + ")") + " AS Number) AS " + colPrefix2.Replace(".", "") + "col" + index;
                    }
                    else
                    {
                        s1 += ", CAST(TO_CHAR(" + colPrefix1 + "\"" + cc.Column1 + "\"" + ") AS Number) AS " + colPrefix1.Replace(".", "") + "col" + index;
                        s2 += ", CAST(TO_CHAR(" + colPrefix2 + "\"" + cc.Column2 + "\"" + ") AS Number) AS " + colPrefix2.Replace(".", "") + "col" + index;
                    }
                    break;
                default:
                    if (isClearSpace)
                    {
                        s1 += ", " + OracleRemoveSpaces("TO_CHAR(" + colPrefix1 + "\"" + cc.Column1 + "\"" + ")") + " AS " + colPrefix1.Replace(".", "") + "col" + index;
                        s2 += ", " + OracleRemoveSpaces("TO_CHAR(" + colPrefix2 + "\"" + cc.Column2 + "\"" + ")") + " AS " + colPrefix2.Replace(".", "") + "col" + index;
                    }
                    else
                    {
                        s1 += ", TO_CHAR(" + colPrefix1 + "\"" + cc.Column1 + "\"" + ") AS " + colPrefix1.Replace(".", "") + "col" + index;
                        s2 += ", TO_CHAR(" + colPrefix2 + "\"" + cc.Column2 + "\"" + ") AS " + colPrefix2.Replace(".", "") + "col" + index;
                    }
                    break;
            }

            return new Tuple<string, string>(s1, s2);
        }
        string ClearProvider(string sConnectionString)
        {
            //Data Source=TMFDEV1;User Id=TMFDGT;Password=goXHUW9LASvPYta
            //Provider=OraOLEDB.Oracle.1;Password=goXHUW9LASvPYta;Persist Security Info=True;User ID=TMFDGT;Data Source=TMFDEV1
            sConnectionString = sConnectionString.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");
            var iStart = sConnectionString.ToLower().IndexOf("provider=");
            if (iStart < 0)
            {
                return sConnectionString;
            }
            var iEnd = sConnectionString.ToUpper().IndexOf(";", iStart) + 1;
            var res = sConnectionString.Remove(iStart, iEnd - iStart);
            return res;
        }
        string OracleRemoveSpaces(string s)
        {
            return "REGEXP_REPLACE(" + s + ",'([[:space:]])')";
        }
        //private void GetAllNoneCompared()
        //{
        //    var sPkColumn = Repository2.PrototypeGenericEntity.PrimaryKeyPropertyComponents.FirstOrDefault() ?? "SID";
        //    var isSets = InfSupSets.ToList();
        //    Parallel.For(0, isSets.Count, i =>
        //    {
        //        var mappedData = new ConcurrentDictionary<ulong, ComparisonEntity>();
        //        IEnumerable<BaseGenericEntity> rd2;
        //        if (i == 0)
        //        {
        //            rd2 = Repository2.GetAll(sPkColumn + " <= '" + isSets[i].Item1 + "'");
        //            rd2.AsParallel().ForAll(x => Map(x, mappedData, KeysMap2));
        //        }
        //        else if (i == isSets.Count - 1)
        //        {
        //            rd2 = Repository2.GetAll(sPkColumn + " >= '" + isSets[i].Item2 + "'");
        //            rd2.AsParallel().ForAll(x => Map(x, mappedData, KeysMap2));
        //        }
        //        else
        //        {
        //            rd2 =
        //                Repository2.GetAll(sPkColumn + " >= '" + isSets[i - 1].Item2 + "' and " + sPkColumn + " <= '" + isSets[i].Item1 + "'");
        //            rd2.AsParallel().ForAll(x => Map(x, mappedData, KeysMap2));
        //        }
        //        var comparedDetailedData = GetDetailedResults(mappedData, new List<BaseGenericEntity>(), rd2);

        //        SaveDetailedResultsToHdd(comparedDetailedData);
        //    });
        //}

        //private async void CompareBatch(IEnumerable<BaseGenericEntity> rd)
        //{
        //    var mappedData = new ConcurrentDictionary<ulong, ComparisonEntity>();

        //    var sPkColumn = Repository2.PrototypeGenericEntity.PrimaryKeyPropertyComponents.FirstOrDefault() ?? "SID";
        //    var sw = new Stopwatch();
        //    sw.Start();
        //    Console.WriteLine("CompareBatch() - Starting map procedure");
        //    rd.AsParallel().ForAll(x => Map(x, mappedData, KeysMap1));

        //    var iMark = sw.ElapsedMilliseconds;
        //    Console.WriteLine("CompareBatch() - Map procedure has taken " + sw.ElapsedMilliseconds);
        //    Console.WriteLine("CompareBatch() - Starting reduce procedure");

        //    var inf = rd.FirstOrDefault() != null ? rd.FirstOrDefault().TryGetProperty(sPkColumn).ToString().Replace("'", "''") : "";
        //    var sup = rd.LastOrDefault() != null ? rd.LastOrDefault().TryGetProperty(sPkColumn).ToString().Replace("'", "''") : "";
        //    InfSupSets.Add(new Tuple<string, string>(inf, sup));

        //    await Task.Yield();
        //    var rd2 = Repository2.GetAll(sPkColumn + " >= '" + inf + "' and " + sPkColumn + " <= '" + sup + "'");
        //    rd2.AsParallel().ForAll(x => Reduce(x, mappedData, KeysMap2));
        //    Console.WriteLine("CompareBatch() - Reduce procedure has taken " + (sw.ElapsedMilliseconds - iMark) + " for " + mappedData.Count + " items.");
        //    iMark = sw.ElapsedMilliseconds;

        //    //get detailed comparison results
        //    //await Task.Yield();
        //    //var comparedDetailedData = GetDetailedResults(mappedData, rd, rd2);
        //    //Console.WriteLine("CompareBatch() - GetDetailedResults() has taken " + (sw.ElapsedMilliseconds - iMark) + " for " + mappedData.Count + " items.");
        //    //iMark = sw.ElapsedMilliseconds;

        //    //save to detail results
        //    await Task.Yield();
        //    SaveDetailedResultsToHdd(mappedData);
        //    Console.WriteLine("CompareBatch() - SaveDetailedResultsToHdd() has taken " + (sw.ElapsedMilliseconds - iMark) + " for " + mappedData.Count + " items.");

        //    sw.Stop();
        //}

        //public ConcurrentDictionary<ulong, DetailedComparisonEntity> GetDetailedResults
        //    (ConcurrentDictionary<ulong, ComparisonEntity> mappedData, IEnumerable<BaseGenericEntity> rd, IEnumerable<BaseGenericEntity> rd2)
        //{
        //    Console.WriteLine("CompareBatch() - GetDetailedResults() has begun.");
        //    var sw = new Stopwatch();
        //    sw.Start();
        //    var res = new ConcurrentDictionary<ulong, DetailedComparisonEntity>();
        //    var des = mappedData.Where(x => x.Value.Result != ComparisonResult.Equal && x.Value.Result != ComparisonResult.NotCompared).ToList();
        //    var rdp1 = rd.AsParallel();
        //    var rdp2 = rd2.AsParallel();

        //    des.AsParallel().ForAll(x =>
        //    {//for each entity that was not equal and was compared generate a details object
        //        //var sw1 = new Stopwatch();
        //        //sw1.Start();
        //        var es1 = rdp1.FirstOrDefault(p => x.Value.Pk == p.GetPk());
        //        var es2 = rdp2.FirstOrDefault(p => x.Value.Pk == p.GetPk());
        //        //Console.WriteLine("CompareBatch() - GetDetailedResults() detailed comparison p1: " + sw1.ElapsedMilliseconds);
        //        //generate the details object
        //        var r = new DetailedComparisonEntity()
        //        {
        //            DiscrepancyDictionary = new Dictionary<string, Tuple<string, string>>(),
        //            Id = x.Value.Id,
        //            Pk = x.Value.Pk,
        //            Result = es1 != null ^ es2 != null ? ComparisonResult.NotCompared : ComparisonResult.NotEqual,
        //            CompareeId = x.Value.CompareeId
        //        };
        //        //Console.WriteLine("CompareBatch() - GetDetailedResults() detailed comparison p2: " + sw1.ElapsedMilliseconds);
        //        if (r.Result == ComparisonResult.NotEqual || r.Result == ComparisonResult.PartiallyEqual)
        //        {
        //            //var disDic = es1.PropertiesDictionary.Except(es2.PropertiesDictionary);
        //            for (int i = 0; i < ComparisonConfig.SourceMapping.Count; i++)
        //            {
        //                var p = ComparisonConfig.SourceMapping[i];
        //                var v1 = es1.TryGetProperty(p.Column1);
        //                var v2 = es2.TryGetProperty(p.Column2);
        //                var s1 = v1 != null ? v1.ToString() : "";
        //                var s2 = v2 != null ? v2.ToString() : "";
        //                if (s1.Length != s2.Length || string.CompareOrdinal(s1, s2) != 0)
        //                {
        //                    r.DiscrepancyDictionary.Add(p.Column1, new Tuple<string, string>(s1, s2));
        //                }

        //            }
        //        }

        //        // Console.WriteLine("CompareBatch() - GetDetailedResults() detailed comparison took: " + sw1.ElapsedMilliseconds);
        //        res.TryAdd(x.Key, r);
        //    });
        //    Console.WriteLine("CompareBatch() - GetDetailedResults() has finished in: " + sw.ElapsedMilliseconds);
        //    return res;
        //}

        //public void SaveDetailedResultsToHdd(ConcurrentDictionary<ulong, DetailedComparisonEntity> comparedDetailedData)
        //{
        //    Console.WriteLine("CompareBatch() - SaveDetailedResultsToHdd() has begun.");
        //    if (!File.Exists("tempRes.txt"))
        //    {
        //        using (File.Create("tempRes.txt")) { }
        //    }
        //    using (var stream = File.Open("tempRes.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
        //    {
        //        foreach (var entity in comparedDetailedData)
        //        {
        //            string s = GenerateDetailedResultInserts(entity);
        //            byte[] bytes = Encoding.UTF8.GetBytes(s);
        //            stream.WriteAsync(bytes, 0, bytes.Length);
        //        }

        //    }
        //    Console.WriteLine("CompareBatch() - SaveDetailedResultsToHdd() has finished.");
        //}
        //private void SaveDetailedResultsToHdd(ConcurrentDictionary<ulong, ComparisonEntity> comparedDetailedData)
        //{
        //    Console.WriteLine("CompareBatch() - SaveDetailedResultsToHdd() has begun.");
        //    if (!File.Exists("tempRes.txt"))
        //    {
        //        using (File.Create("tempRes.txt")) { }
        //    }
        //    using (var stream = File.Open("tempRes.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
        //    {
        //        var bks = comparedDetailedData.Select(x => x.Value.Pk);
        //        for (int i = 0; i < bks.Count(); i += 100)
        //        {
        //            var bs = bks.Skip(i).Take(100);
        //            string s = GenerateDetailedResultInserts(bs.ToList());
        //            byte[] bytes = Encoding.UTF8.GetBytes(s);
        //            stream.WriteAsync(bytes, 0, bytes.Length);
        //        }

        //    }
        //    Console.WriteLine("CompareBatch() - SaveDetailedResultsToHdd() has finished.");
        //}

        //private string GenerateDetailedResultInserts(List<string> bks)
        //{
        //    string sInserts = "";
        //    //generate inserts of the details object
        //    var sb = new StringBuilder();
        //    sb.Append("depts BkList := BkList(");
        //    for (int index = 0; index < bks.Count; index++)
        //    {
        //        var bk = bks[index];
        //        if (index != 0)
        //        {
        //            sb.Append(",'" + bk + "'");
        //        }
        //        else
        //        {
        //            sb.Append("'" + bk + "'");
        //        }
        //    }
        //    sb.Append(");");

        //    /*
        //     * DECLARE
        //           TYPE BkList IS VARRAY(10) OF VARCHAR2(150);
        //           depts BkList := BkList('aa','bb','cc');
        //        BEGIN 
        //           FORALL j IN depts.first .. depts.last 
        //              INSERT INTO TMFENT.F_GOV_DIFF_DISC(BK) VALUES (depts(j));
        //           COMMIT;
        //        END;
        //     */
        //    sInserts += "DECLARE TYPE BkList IS VARRAY(150) OF VARCHAR2(150); " + sb + " BEGIN FORALL j IN depts.first .. depts.last  INSERT INTO " + CompareDiscTable + " (BK) VALUES (depts(j)); COMMIT; END;" + Environment.NewLine;


        //    return sInserts;
        //}

        //private string GenerateDetailedResultInserts(KeyValuePair<ulong, DetailedComparisonEntity> entity)
        //{
        //    string sInserts = "";
        //    //generate inserts of the details object
        //    foreach (var d in entity.Value.DiscrepancyDictionary)
        //    {
        //        sInserts += "INSERT INTO " + DetailCompareSumTable + " (Line_of_Business, Claims_Manager,COMPARISON_ID, ADT_DATE, Table_1_Name, Table_2_Name, TABLE_1_BATCHID, "
        //        + "TABLE_2_BATCHID,Entity_1_BK, Entity_2_BK, Field,Entity_1_Val, Entity_2_Val) VALUES ('No LOB', 'No CM', '" + ComparisonId + "','" + DateTime.Now + "', "
        //        + " '" + Repository1.FromSource + "', '" + Repository2.FromSource + "', 'No T1 batch id', 'No T2 batch id', '" + (d.Value.Item1.IsNotNullOrEmpty() ? entity.Value.Pk : "") + "',"
        //        + " '" + (d.Value.Item2.IsNotNullOrEmpty() ? entity.Value.Pk : "") + "', '" + d.Value.Item1 + "', '" + d.Value.Item2 + "');" + Environment.NewLine;
        //    }

        //    return sInserts;
        //}

        //public async void Map(IGenericEntity obj, ConcurrentDictionary<ulong, ComparisonEntity> mappedData, List<string> keys)
        //{
        //    if (obj == null)
        //    {
        //        return;
        //    }

        //    MapReduceRunCount++;
        //    var compareeHash = keys.Any() ? obj.GetHashCode(keys) : obj.GetHashCode();
        //    var newCe = CreateComparisonEntity(obj, compareeHash, obj.GetId());
        //    InsertComparisonEntity(newCe, mappedData);

        //    //Console.WriteLine("Map() - Entity ID: " + objId + " Itteration: " + MapReduceRunCount + " time taken: " + sw.ElapsedMilliseconds);
        //}
        //public async void Reduce(IGenericEntity obj, ConcurrentDictionary<ulong, ComparisonEntity> mappedData, List<string> keys)
        //{
        //    if (obj == null)
        //    {
        //        return;
        //    }

        //    MapReduceRunCount++;
        //    var objId = obj.GetId();
        //    var mappedItem = await GetComparisonEntity(objId, mappedData);//classic linq -> too slow

        //    var compareeHash = keys.Any() ? obj.GetHashCode(keys) : obj.GetHashCode();
        //    if (mappedItem != null)
        //    {//map into an existing object
        //        var bIsNewHash = false;
        //        for (var i = 0; i < mappedItem.ComparedHashedEntities.Count; i++)
        //        {
        //            var he = mappedItem.ComparedHashedEntities[i];
        //            mappedItem.Result = GetComparisonResult(he, compareeHash, mappedItem.Result);
        //            if (mappedItem.Result == ComparisonResult.NotEqual || mappedItem.Result == ComparisonResult.PartiallyEqual)
        //            {//in the case of many data sources this can have some usless itterations due to inefficient check right here
        //                //TODO: improve efficiency
        //                bIsNewHash = true;
        //            }
        //        }
        //        if (bIsNewHash)
        //        {
        //            mappedItem.ComparedHashedEntities.Add(compareeHash);
        //        }
        //    }
        //    else
        //    {//map into a new object - none existed
        //        var newCe = CreateComparisonEntity(obj, compareeHash, objId);
        //        InsertComparisonEntity(newCe, mappedData);
        //    }

        //    //Console.WriteLine("Reduce() - Entity ID: " + objId + " Itteration: " + MapReduceRunCount + " time taken: " + sw.ElapsedMilliseconds);
        //}

        //private ComparisonEntity CreateComparisonEntity(IGenericEntity obj, ulong compareeHash, ulong objId)
        //{
        //    var newCe = new ComparisonEntity { Id = Guid.NewGuid(), CompareeId = objId, Pk = obj.GetPk(), Result = ComparisonResult.NotCompared };
        //    newCe.ComparedHashedEntities = new List<ulong> { compareeHash };
        //    return newCe;
        //}

        //private ComparisonResult GetComparisonResult(ulong hash1, ulong hash2, ComparisonResult currentResult)
        //{
        //    if (hash1 == hash2)
        //    {
        //        return (currentResult == ComparisonResult.NotCompared || currentResult == ComparisonResult.Equal)
        //            ? ComparisonResult.Equal : ComparisonResult.PartiallyEqual;
        //    }
        //    return (currentResult == ComparisonResult.NotCompared || currentResult == ComparisonResult.NotEqual)
        //        ? ComparisonResult.NotEqual : ComparisonResult.PartiallyEqual;
        //}

        //private async Task<ComparisonEntity> GetComparisonEntity(ulong id, ConcurrentDictionary<ulong, ComparisonEntity> mappedData)
        //{
        //    ComparisonEntity ce = null;
        //    mappedData.TryGetValue(id, out ce);
        //    return ce;
        //}
        //private async void InsertComparisonEntity(ComparisonEntity ce, ConcurrentDictionary<ulong, ComparisonEntity> mappedData)
        //{
        //    mappedData.TryAdd(ce.CompareeId, ce);
        //}
    }
}
