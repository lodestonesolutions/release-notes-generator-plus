﻿using System;
using Castle.Core;

namespace Pattern.IoC.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class RegisterAttribute : Attribute
    {
        protected Type ServiceType { get; set; }
        public string Key { get; set; }
        public LifestyleType LifeStyle = LifestyleType.Singleton;
        public bool IsDefault { get; set; }

        public RegisterAttribute() { }

        public RegisterAttribute(string key)
            : this()
        {
            Key = key;
        }

        public RegisterAttribute(Type serviceType)
            : this()
        {
            ServiceType = serviceType;
        }

        public RegisterAttribute(string key, Type serviceType)
            : this(serviceType)
        {
            Key = key;
        }
    }
}
