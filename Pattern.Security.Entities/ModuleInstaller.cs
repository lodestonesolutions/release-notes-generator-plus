﻿using System;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using log4net;
using NHibernate;
using Pattern.Entities.Enums;
using Pattern.Security.Entities.Interfaces;
using Pattern.Security.Entities.Repository;

namespace Pattern.Security.Entities
{
    public class ModuleInstaller : IWindsorInstaller
    {
        private string _sessionContext;
        private readonly RegistrationLifestyle _registrationLifestyle;
        private readonly ILog _log = LogManager.GetLogger(typeof(ModuleInstaller));

        public ModuleInstaller(RegistrationLifestyle registrationLifestyle)
        {
            _registrationLifestyle = registrationLifestyle;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            LifestyleType ls;

            switch (_registrationLifestyle)
            {
                case RegistrationLifestyle.Web:
                    ls = LifestyleType.PerWebRequest;
                    break;
                case RegistrationLifestyle.WebApi:
                    ls = LifestyleType.Scoped;
                    break;
                case RegistrationLifestyle.Scoped:
                    ls = LifestyleType.Scoped;
                    break;
                default:
                    ls = LifestyleType.Transient;
                    break;
            }

            _sessionContext = GetSessionContext(_registrationLifestyle);
            ConfigureNHibernate(container, ls);

            if (_log.IsInfoEnabled) _log.Info("Pattern.Security.Entities installer complete");
        }
        protected virtual void ConfigureNHibernate(IWindsorContainer container, LifestyleType ls)
        {
            try
            {
                var configuration = Fluently.Configure().Database(MsSqlConfiguration.MsSql2012
                    .ConnectionString(x => x.FromConnectionStringWithKey(DataContext.ConnectionStringName)))
                    .Cache(c => c.ProviderClass<NHibernate.Caches.SysCache.SysCacheProvider>().UseSecondLevelCache().UseQueryCache())
                    .CurrentSessionContext(_sessionContext)
                    .Mappings(m => m.FluentMappings.AddFromAssembly(GetType().Assembly))
                    .BuildConfiguration();

                container.Register(Component.For<ISessionFactory>().Instance(configuration.BuildSessionFactory())
                    .Named("sf.security"));

                container.Register(Component.For(typeof(ISecurityRepository<,>)).ImplementedBy(typeof(SecurityRepository<,>))
                    .DependsOn(Dependency.OnComponent(typeof(ISessionFactory), "sf.security"))
                    .LifeStyle.Is(ls));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        private static string GetSessionContext(RegistrationLifestyle self)
        {
            switch (self)
            {
                case RegistrationLifestyle.Web:
                    return "web";
                case RegistrationLifestyle.Application:
                    return "call";
                case RegistrationLifestyle.Scoped:
                    return "call";
                default:
                    return "thread_static";
            }
        }
    }
}
