﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net;
using Pattern.Services.Implementations;
using Pattern.Services.Implementations.Config;
using Pattern.Services.Implementations.Mock;
using Pattern.Services.Interfaces;

namespace Pattern.Services
{
    public class ModuleInstaller : IWindsorInstaller
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModuleInstaller));

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IConfigurationService>().ImplementedBy<GeneralConfigurationService>());

            #if DEBUG
                container.Register(Component.For<IMailService>().ImplementedBy<MockMailService>());
                _log.Info("Registered MockMailService : IMailService");
            #else
                container.Register(Component.For<IMailService>().ImplementedBy<MailService>());
                _log.Info("Registered MailService : IMailService");
            #endif

            container.Register(Castle.MicroKernel.Registration.Classes.FromThisAssembly()
               .Pick().If(Component.IsCastleComponent));

            if (_log.IsInfoEnabled)
                _log.Info("SICorp.Services installer complete");
        }
    }
}
