﻿namespace Pattern.Services.Jira.Domain
{
    public class IssueType: AddressableNamedEntity
    {
        public bool Subtask { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
