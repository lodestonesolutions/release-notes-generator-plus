﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net;
using NHibernate;
using PatternPlayground.Repository;

namespace Pattern.Repository.NHibernate
{
    public class ModuleInstaller : IWindsorInstaller
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModuleInstaller));

        public ModuleInstaller()
        {

        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {          
            //container.Register(Component.For(typeof(IRepository<,>)).ImplementedBy(typeof(NHibernateRepository<,>))
            //    .DependsOn(Dependency.OnComponent(typeof(ISessionFactory), "sf.dg")));
            //container.Register(Component.For(typeof(IOrmRepository<,>)).ImplementedBy(typeof(NHibernateRepository<,>))
            //    .DependsOn(Dependency.OnComponent(typeof(ISessionFactory), "sf.dg")));
            if (_log.IsInfoEnabled) _log.Info("Pattern.Repository.NHibernate installer complete");
        }
    }
}
