﻿using System;
using System.Text;
using CityHash;

namespace Pattern.Hash.CityHash
{
    public static class CityHash
    {
        public static UInt32 GetCityHash32(this string value)
        {
            return CityHashExtension.GetCityHash32(value);
        }

        public static UInt64 GetCityHash64(this string value)
        {
            return CityHashExtension.GetCityHash64(value);
        }

        
    }
}
