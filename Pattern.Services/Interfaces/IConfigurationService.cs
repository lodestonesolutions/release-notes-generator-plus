﻿namespace Pattern.Services.Interfaces
{
    public interface IConfigurationService
    {
        T GetAppSettingValue<T>(string key, T defaultVal);
        T GetAppSettingValue<T>(string key);
        string GetAppSettingValue(string key);
        string GetAppSettingValue(string key, string defaultVal);
    }
}
