﻿namespace Pattern.Services.Jira.Domain
{
    public abstract class AddressableNamedEntity
    {
        public string Name { get; set; }
        public string Self { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
