﻿namespace Pattern.Services.Jira.Domain
{
    public class Project: AddressableNamedEntity
    {
        public string Key { get; set; }
    }
}
