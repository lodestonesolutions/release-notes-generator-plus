﻿using System;
using System.Runtime.Caching;
using Castle.Core;
using Pattern.Services.Interfaces;

namespace Pattern.Services.Implementations
{
    [CastleComponent(typeof(ICacheService))]
    public class MemoryCacheService : ICacheService
    {
        private readonly ObjectCache _cache = MemoryCache.Default;
        
        public object Get(string key)
        {
            try
            {
                return _cache.Get(key);
            }
            catch { }

            return null;
        }

        public void Set(string key, object value, int cacheSeconds)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key)
        {
            try
            {
                return (T)_cache.Get(key);
            }
            catch { }

            return default(T);
        }

        public void Set<T>(string key, T value, int cacheSeconds)
        {
            var policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(cacheSeconds) };
            _cache.Set(key, value, policy);
        }

        public void Set<T>(string key, T value, TimeSpan slidingExpiration)
        {
            var policy = new CacheItemPolicy { SlidingExpiration = slidingExpiration };
            _cache.Set(key, value, policy);
        }

        public void Invalidate(string key)
        {
            try
            {
                if (_cache.Contains(key))
                    _cache.Remove(key);
            }
            catch { }
        }
    }
}
