﻿using System.Collections.Generic;
using System.Linq;
using Pattern.Entities;

namespace Pattern.Extensions
{
    public static class IGenericEntityExtensions
    {
        public static IList<string> GetProperties(this IGenericEntity ge)
        {
            return ge.PropertiesDictionary.Select(item => item.Key).ToList();
        }
    }
}
