﻿namespace Pattern.Services.Jira.Constants
{
    static class AppSettingsKeys
    {
        public const string JiraServiceBaseUrl = "JiraServiceBaseUrl";
        public const string JiraServiceAuthUrl = "JiraServiceAuthUrl";
        public const string JiraUsername = "JiraUsername";
        public const string JiraPw = "JiraPw";
    }
}
