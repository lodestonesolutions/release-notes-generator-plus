﻿using System.Collections.Generic;

namespace Pattern.IoC.ServiceLocator
{
    public class ServiceLocator : IServiceLocator
    {
        public TService GetInstance<TService>()
        {
            var service = Container.Instance.Resolve<TService>();
            Container.Instance.Release(service);
            return service;
        }

        public TService GetInstance<TService>(string key)
        {
            var service = Container.Instance.Resolve<TService>(key);
            Container.Instance.Release(service);
            return service;
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            var service = Container.Instance.Resolve<IEnumerable<TService>>();
            Container.Instance.Release(service);
            return service;
        }
    }
}
