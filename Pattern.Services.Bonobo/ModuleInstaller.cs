﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Pattern.Services.Bonobo.Interfaces;
using Pattern.Services.Bonobo.Services;

namespace Pattern.Services.Bonobo
{
    public class ModuleInstaller : IWindsorInstaller
    {
        public ModuleInstaller()
        {

        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IBonoboQueryService>().ImplementedBy<BonoboQueryService>());
        }

        
    }
}
