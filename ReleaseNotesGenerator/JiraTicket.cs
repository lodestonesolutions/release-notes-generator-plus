﻿using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;

namespace ReleaseNotesGenerator
{
    
    public class JiraTicket
    {
        public JiraTicket()
        {
            Attachments = new List<string>();
        }

        public string JiraKey { get; set; }
        public string Title { get; set; }
        public string ExpectedChange { get; set; }
        public string FilesAffected { get; set; }
        public string CodeSamples { get; set; }
        public string Description { get; set; }
        public List<string> Attachments { get; set; }
        
    }
}