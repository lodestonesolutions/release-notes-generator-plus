﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Pattern.Repository.NHibernate.Common.Interfaces;

namespace Pattern.Repository
{
    public interface IOrmRepository<TEntity, in TKey> : IRepository<TEntity, TKey> where TEntity : class
    {

        TEntity Load(TKey id);
        TEntity Get(Expression<Func<TEntity, bool>> whereFunc, Expression<Func<TEntity, object>> fetch = null);
        TEntity Get(Expression<Func<TEntity, bool>> whereFunc, List<Expression<Func<TEntity, object>>> fetches);
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, int take);
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, Expression<Func<TEntity, object>> fetch = null, int take = -1);
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, List<Expression<Func<TEntity, object>>> fetches, int take = -1);
        IEnumerable<TEntity> GetAllPaged(Expression<Func<TEntity, bool>> whereFunc, int pageNum, int pageSize, Expression<Func<TEntity, object>> fetch = null);
        IEnumerable<TEntity> GetAllPaged(Expression<Func<TEntity, bool>> whereFunc, int pageNum, int pageSize, List<Expression<Func<TEntity, object>>> fetches);
        int GetCount(Expression<Func<TEntity, bool>> whereFunc = null);
        IUnitOfWork BeginUnitOfWork();
        void Save(List<TEntity> entities);
        void Delete(List<TEntity> entities);
    }
}
