﻿using System;
using NHibernate;

namespace Pattern.Repository.NHibernate.Common.Interfaces
{
    public interface IStatelessUnitOfWorkFactory : IDisposable
    {
        IStatelessUnitOfWork Create(IStatelessSession session);
        /// <summary>
        /// releases the specified the unitofwork
        /// </summary>
        /// <param name="unitOfWork"></param>
        void Release(IStatelessUnitOfWork unitOfWork);
        /// <summary>
        /// Determine if the specified unitofwork object is in the context of an active transaction
        /// </summary>
        /// <param name="uow"></param>
        /// <returns></returns>
        bool IsRoot(IStatelessUnitOfWork uow);
        /// <summary>
        /// Gets the currently active transaction
        /// </summary>
        /// <returns></returns>
        ITransaction GetCurrentTransaction();

        /// <summary>
        /// The unique id of the factory
        /// </summary>
        Guid Id { get; }
    }
}
