﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Pattern.IoC;
using Pattern.Security.Entities.Entities;
using Pattern.Security.Interfaces;
using Pattern.Services.Interfaces;

namespace Pattern.Security.Services
{
    public class MockWindowsSecurityService : IWindowsSecurityService
    {
        public WindowsIdentity GetUserPricipal()
        {
            return WindowsIdentity.GetCurrent();
        }

        public string GetUserName()
        {
            return "mockUser";
        }

        public User GetUser(int userId)
        {
            var roles = GetUserRoles(userId.ToString());
            return new User
            {
                CreatedBy = "mockSystem",
                CreatedOn = DateTime.Now,
                Id = 1,
                IsActive = true,
                ModifiedBy = "mockSystem",
                ModifiedOn = DateTime.Now,
                Username = "mockUser",
                Roles = roles
            };
        }

        public User GetUser(string username)
        {
            var roles = GetUserRoles(username);
            return new User
            {
                CreatedBy = "mockSystem",
                CreatedOn = DateTime.Now,
                Id = 1,
                IsActive = true,
                ModifiedBy = "mockSystem",
                ModifiedOn = DateTime.Now,
                Username = "mockUser",
                Roles = roles
            };
        }

        public IList<Role> GetUserRoles(string username)
        {
            var acts = GetUserActivities(username);
            var roles = new List<Role> { new Role { RoleName = "mockAdmin",Activities = acts} };
            return roles;
        }

        public IList<Activity> GetUserActivities(string username)
        {
            var configurationService = Container.Instance.Resolve<IConfigurationService>();
            var activities = configurationService.GetAppSettingValue("mockActivities");

            if (activities.IsNullOrEmpty())
            {
                var acts = new List<Activity>();
                var a1 = new Activity { Id = 11, ActivityName = "Approve" };
                var a2 = new Activity { Id = 22, ActivityName = "CreateNewUploadDelete" };
                var a3 = new Activity { Id = 33, ActivityName = "EditTransaction" };
                var a4 = new Activity { Id = 44, ActivityName = "Execute" };
                var a5 = new Activity { Id = 55, ActivityName = "Read" };
                var a6 = new Activity { Id = 66, ActivityName = "ConfigMetadata" };
                var a7 = new Activity { Id = 77, ActivityName = "UserManagement" };

                acts.Add(a1);
                acts.Add(a2);
                acts.Add(a3);
                acts.Add(a4);
                acts.Add(a5);
                acts.Add(a6);
                acts.Add(a7);

                return acts;
            }
            else
            {
                var lst = new List<Activity>();
                var acts = activities.Split(';');
                var i = 0;
                foreach (var act in acts)
                {
                    lst.Add(new Activity { Id = i, ActivityName = act });
                    i++;
                }
                return lst;
            }
        }
    }
}
