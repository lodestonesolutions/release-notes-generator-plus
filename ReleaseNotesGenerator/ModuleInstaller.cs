﻿using System.Web;

using Castle.Components.DictionaryAdapter;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net;


namespace ReleaseNotesGenerator
{
    public class ModuleInstaller : IWindsorInstaller
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModuleInstaller));

        public ModuleInstaller()
        {

        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            
        }

        
    }
}
