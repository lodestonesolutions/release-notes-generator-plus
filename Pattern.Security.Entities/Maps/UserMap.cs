﻿using FluentNHibernate.Mapping;
using Pattern.Security.Entities.Constants;
using Pattern.Security.Entities.Entities;

namespace Pattern.Security.Entities.Maps
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table(TableNames.UserTable);

            Id(x => x.Id, "SID").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.Username, "USERNAME").Not.Nullable();
            Map(x => x.IsActive, "ISACTIVE").Not.Nullable();

            Map(x => x.CreatedOn, "CREATEDON").Not.Nullable();
            Map(x => x.CreatedBy, "CREATEDBY").Not.Nullable();
            Map(x => x.ModifiedOn, "MODIFIEDON").Not.Nullable();
            Map(x => x.ModifiedBy, "MODIFIEDBY").Not.Nullable();

            HasManyToMany(x => x.Roles).Not.LazyLoad().ParentKeyColumn("USER_FK").ChildKeyColumn("ROLE_FK");
        }
    }
}
