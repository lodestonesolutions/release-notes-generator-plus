﻿using System;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NHibernate;
using NHibernate.Context;
using NHibernate.Dialect.Schema;
using NHibernate.Exceptions;
using Pattern.Repository.NHibernate.Common.Extensions;
using Pattern.Repository.NHibernate.Common.Interfaces;

namespace Pattern.Repository.NHibernate.Common.UnitOfWork
{
    public class DefaultStatelessUnitOfWork : IStatelessUnitOfWork
    {
        private bool _disposed;

        private readonly ILog _log = LogManager.GetLogger(typeof(DefaultStatelessUnitOfWork));
        private readonly ITransaction _transaction;
        private readonly ISessionFactory _sessionFactory;

        public IStatelessSession Session { get; private set; }
        public Guid Id { get; private set; }

        public ITransaction Transaction
        {
            get { return _transaction; }
        }

        public DefaultStatelessUnitOfWork(ISessionFactory sessionFactory, IsolationLevel isolationLevel = IsolationLevel.ReadUncommitted)
        {
            Id = Guid.NewGuid();
            _sessionFactory = sessionFactory;

            if (CurrentSessionContext.HasBind(sessionFactory))
                throw new ApplicationException("Session is already bound!");

            Session = sessionFactory.OpenStatelessSession();
            //Session.FlushMode = FlushMode.Commit;

            if (System.Transactions.Transaction.Current == null)
                _transaction = Session.BeginTransaction(isolationLevel);

            //CurrentSessionContext.Bind(Session);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;

            try
            {
                if (_transaction != null)
                {
                    if (_transaction.IsActive && !_transaction.WasCommitted && !_transaction.WasRolledBack)
                        Rollback();

                    _transaction.Dispose();
                }
            }
            catch (Exception ex)
            {
                _log.Fatal("Error disposing transaction", ex);
                //throw;
            }
            finally
            {
                try
                {
                    if (Session != null)
                        Session.Dispose();
                }
                finally
                {
                    CurrentSessionContext.Unbind(_sessionFactory);
                    GC.SuppressFinalize(this);
                }
            }
        }

        public void Commit()
        {
            if (!_transaction.IsActive)
                return;

            try
            {
                if (_log.IsDebugEnabled)
                    //_log.DebugFormat("UoW_Commit [{0}]-S[{1}]", Id, Session.GetSessionId());

                _transaction.Commit();
            }
            catch (Exception ex)
            {
                _log.Fatal("Error committing transaction", ex);
                throw;
            }
        }

        public void Rollback()
        {
            if (!_transaction.IsActive)
                return;

            try
            {
                if (_log.IsDebugEnabled)
                    //_log.DebugFormat("UoW_Rolback[{0}]-S[{1}]", Id, Session.GetSessionId());

                _transaction.Rollback();
            }
            catch (Exception ex)
            {
                _log.Fatal("Error rolling back transaction", ex);
                throw;
            }
        }

        ~DefaultStatelessUnitOfWork()
        {
            Dispose();
        }
    }
}
