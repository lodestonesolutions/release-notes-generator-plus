﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq.Expressions;
using Pattern.Extensions;

namespace Pattern.Repository.ADO
{
    public class AdoRepository<TEntity, TKey>
        : IRepository<TEntity, TKey> where TEntity : class
    {
        protected DbConnection Connection;
        public AdoRepository()
        {
            //create any other provider factory.. for Oracle, MySql, etc...
            //TODO: Generic factory
            var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            Connection = factory.CreateConnection(); // will return the connection object, in this case, SqlConnection ...

            if (Connection == null)
            {
                //TODO: Log
                return;
                
            }

            //Connection.ConnectionString = "read connection string from somewhere.. .config file for sample";
        }

        public TEntity Get(TKey id)
        {
            TEntity res = null;
            try
            {
                Connection.Open();

                // create command to execute queries...
                var command = Connection.CreateCommand(); // create a SqlCommand, OracleCommand etc... depende of the provider factory configured.
                command.CommandText = "SQL command here";
                var reader = command.ExecuteReader();

                res = reader.MapDataToEntity<TEntity>();
                // some logic 
            }
            catch
            {
            }
            finally
            {
                //close connection
                Connection.Close();
            }

            return res;
        }

        public IEnumerable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IEnumerable<TEntity>> GetAllYield()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> func, Expression<Func<TEntity, object>> fetch = null)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereFunc, List<Expression<Func<TEntity, object>>> fetches)
        {
            throw new NotImplementedException();
        }

        public TEntity Create(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Save(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //TODO:Implement
            //throw new NotImplementedException();
        }
    }
}
