﻿using Castle.Windsor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pattern.Entities.Enums;

namespace Pattern.Security.UnitTest
{
    [TestClass]
    public static class Bootstrap
    {
        [AssemblyInitialize]
        public static void Initialize(TestContext context)
        {
            var wc = new WindsorContainer();
            IoC.Container.Instance = wc;

            wc.Install(
                new IoC.ModuleInstaller(),
                new Pattern.Repository.NHibernate.ModuleInstaller(),
                new Pattern.Security.Entities.ModuleInstaller(RegistrationLifestyle.Application),
                new DataGovernance.Entities.ModuleInstaller(RegistrationLifestyle.Application)
            );
        }

        [AssemblyCleanup]
        public static void Cleanup()
        {
            IoC.Container.Instance.Dispose();
        }
    }
}
