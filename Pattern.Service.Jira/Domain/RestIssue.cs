﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pattern.Extensions;
using Pattern.Services.Jira.Constants;

namespace Pattern.Services.Jira.Domain
{
    public class RestIssue : Issue
    {
        public string expand { get; set; }
        public string id { get; set; }
        public override string Self { get; set; }
        public override string Key { get; set; }
        public Fields fields { get; set; }
        public override IList<Comment> Comments
        {
            get
            {
                if (fields != null && fields.comment != null)
                {
                    return fields.comment.comments;
                }
                return null;
            }
            set
            {
                if (fields != null && fields.comment != null)
                {
                    fields.comment.comments = value.ToList();
                }
                else
                {
                    fields = new Fields();
                    fields.comment = new Comments();
                    fields.comment.comments = value.ToList();
                }
            }
        }

        public override Status Status
        {
            get { return fields != null ? fields.status : null; }
            set
            {
                if (fields == null)
                {
                    fields = new Fields();
                }
                fields.status = value;
            }
        }
        public override DateTime? Updated
        {
            get { return GetJavaDateTime(fields.updated); }
        }
    }

    public class Issuetype
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public bool subtask { get; set; }
    }

    public class Watches
    {
        public string self { get; set; }
        public int watchCount { get; set; }
        public bool isWatching { get; set; }
    }

    public class Creator
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    public class Reporter
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    public class Aggregateprogress
    {
        public int progress { get; set; }
        public int total { get; set; }
    }

    public class Progress
    {
        public int progress { get; set; }
        public int total { get; set; }
    }


    public class Author
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    public class UpdateAuthor
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    public class Votes
    {
        public string self { get; set; }
        public int votes { get; set; }
        public bool hasVoted { get; set; }
    }

    public class Worklog
    {
        public int startAt { get; set; }
        public int maxResults { get; set; }
        public int total { get; set; }
        public List<object> worklogs { get; set; }
    }

    public class Assignee
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    public class StatusCategory
    {
        public string self { get; set; }
        public int id { get; set; }
        public string key { get; set; }
        public string colorName { get; set; }
        public string name { get; set; }
    }
    public class Comments
    {
        public int total { get; set; }
        public List<Comment> comments { get; set; }
    }

    public class IssueLinks
    {
        public string self { get; set; }
        public int id { get; set; }
        public object type { get; set; }
        OutwardIssue outwardIssue { get; set; }
    }

    public class OutwardIssue
    {
        public string self { get; set; }
        public int id { get; set; }
        public string key { get; set; }
        IssueType issuetype { get; set; }
        Priority priority { get; set; }

    }

    public class Fields
    {
        public Issuetype issuetype { get; set; }
        public List<JToken> components { get; set; }
        public JToken timespent { get; set; }
        public JToken timeoriginalestimate { get; set; }
        public JToken description { get; set; }
        public Project project { get; set; }
        public List<JToken> fixVersions { get; set; }
        public JToken aggregatetimespent { get; set; }
        public JToken resolution { get; set; }
        public List<JToken> attachment { get; set; }
        public object aggregatetimeestimate { get; set; }
        public object resolutiondate { get; set; }
        public int workratio { get; set; }
        public string summary { get; set; }
        public string lastViewed { get; set; }
        public Watches watches { get; set; }
        public Creator creator { get; set; }
        public List<object> subtasks { get; set; }
        public string created { get; set; }
        public Reporter reporter { get; set; }
        public Aggregateprogress aggregateprogress { get; set; }
        public Priority priority { get; set; }
        public List<JToken> labels { get; set; }
        public object environment { get; set; }
        public object timeestimate { get; set; }
        public object aggregatetimeoriginalestimate { get; set; }
        public List<JToken> versions { get; set; }
        public object duedate { get; set; }
        public Progress progress { get; set; }
        public Comments comment { get; set; }
        public List<JToken> issuelinks { get; set; }
        public Votes votes { get; set; }
        public Worklog worklog { get; set; }
        public Assignee assignee { get; set; }
        public string updated { get; set; }
        public Status status { get; set; }
    }

}
