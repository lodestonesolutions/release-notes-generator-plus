﻿using System;
using System.Collections.Generic;

namespace Pattern.Entities
{
    public interface IGenericEntity
    {//essentially a weak expando object
        Dictionary<string, Tuple<object, Type>> PropertiesDictionary { get; set; }
        List<string> PrimaryKeyPropertyComponents { get; set; }
        /// <summary>
        /// The source (table, view, entity, etc..) the data for this object was taken from
        /// </summary>
        string EntitySource { get; set; }
        /// <summary>
        /// Should return a hashed Id at best or a serialized Id at worst.
        /// </summary>
        /// <returns></returns>
        ulong GetId();

        string GetPk();
        void RemoveProperty(string key);
        /// <summary>
        /// Inserts a new property or updates an existing one into the entity property.
        /// If used to update no type is needed (defaults to null) if used to set a new property a type must be provided
        /// </summary>
        /// <param name="key">Property Name</param>
        /// <param name="value">Property Value</param>
        /// <param name="type">objects type</param>
        void SetProperty(string key, object value, Type type = null);
        object TryGetProperty(string key);
        Type TryGetPropertyType(string key);
        bool IsEqual(IGenericEntity obj);
        ulong GetHashCode();
        ulong GetHashCode(List<string> keysOrder);
    }
}
