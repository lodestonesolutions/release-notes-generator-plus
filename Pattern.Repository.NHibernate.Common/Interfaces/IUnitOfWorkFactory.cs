﻿using System;
using NHibernate;

namespace Pattern.Repository.NHibernate.Common.Interfaces
{
    public interface IUnitOfWorkFactory : IDisposable
    {
        IUnitOfWork Create(ISession session);
        /// <summary>
        /// releases the specified the unitofwork
        /// </summary>
        /// <param name="unitOfWork"></param>
        void Release(IUnitOfWork unitOfWork);
        /// <summary>
        /// Determine if the specified unitofwork object is in the context of an active transaction
        /// </summary>
        /// <param name="uow"></param>
        /// <returns></returns>
        bool IsRoot(IUnitOfWork uow);
        /// <summary>
        /// Gets the currently active transaction
        /// </summary>
        /// <returns></returns>
        ITransaction GetCurrentTransaction();

        /// <summary>
        /// The unique id of the factory
        /// </summary>
        Guid Id { get; }
    }
}
