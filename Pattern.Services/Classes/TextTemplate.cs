﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern.Services.Classes
{
    public class TextTemplate
    {
        private readonly List<TextToken> _tokens = new List<TextToken>();

        public TextTemplate(string template)
        {
            Template = template;
        }

        public string Template { get; private set; }

        public string Process()
        {
            if (_tokens.Count == 0)
                return Template;

            var sb = new StringBuilder(Template);

            foreach (var token in _tokens)
            {
                sb.Replace(string.Format("{{#{0}#}}", token.Key), token.Value);
            }

            return sb.ToString();
        }

        public void AddToken(TextToken token)
        {
            _tokens.Add(token);
        }

        public void AddTokens(params TextToken[] tokens)
        {
            _tokens.AddRange(tokens);
        }

        public void AddTokens(IList<TextToken> tokens)
        {
            _tokens.AddRange(tokens);
        }

        public void Clear()
        {
            _tokens.Clear();
        }
    }
}
