﻿namespace Pattern.Services.Interfaces
{
    public interface IRazorTemplateService
    {
        string ParseTemplate<T>(string templateFilename, T model, string cacheName);
        string ParseTemplate<T>(string templateFilename, T model);
    }
}
