﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net;
using Pattern.Services.Implementations.Mock;
using Pattern.Services.Interfaces;
using Pattern.Services.Jira.Interfaces;
using Pattern.Services.Jira.Services;

namespace Pattern.Services.Jira
{
    public class ModuleInstaller : IWindsorInstaller
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModuleInstaller));

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

#if DEBUG
            //container.Register(Component.For<IJiraAccessService>().ImplementedBy<MockJiraAccessService>());
            container.Register(Component.For<IJiraAccessService>().ImplementedBy<JiraAccessService>());
            _log.Info("Registered MockJiraAccessService : IJiraAccessService");
            #else
                container.Register(Component.For<IJiraAccessService>().ImplementedBy<JiraAccessService>());
                _log.Info("Registered JiraAccessService : IJiraAccessService");
            #endif
        }
    }
}
