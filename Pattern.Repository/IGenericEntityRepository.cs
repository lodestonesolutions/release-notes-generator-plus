﻿using System;
using System.Collections.Generic;
using System.Data;

namespace PatternPlayground.Repository
{
    public interface IGenericEntityRepository<TEntity, in TKey> : IDisposable where TEntity : class
    {
        IDbConnection Connection { get; set; }
        TEntity PrototypeGenericEntity { get; set; }
        string SourceName { get; set; }//TABLE NAME
        TEntity Get(TKey id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<IEnumerable<TEntity>> GetAllYield();
        TEntity Create(TEntity entity);
        void Update(TEntity entity);
        void Save(TEntity entity);
        void Delete(TEntity entity);
    }
}
