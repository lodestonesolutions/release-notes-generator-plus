﻿using Newtonsoft.Json.Linq;
using Pattern.Services.Jira.Domain;

namespace Pattern.Services.Jira.Interfaces
{
    public interface IJiraAccessService
    {
        Issue GetIssue(string key);
        JToken GetIssueAsJToken(string key);
        JToken GetFilterAsJToken(string filterId);
        JToken AccessApiAsJToken(string restQueryString);
        void UpdateIssue(Issue issue,string key);
        void UpdateStatus(string status, string key);
        void AddComment(string comment, string key);
        Issue CreateIssue(string summary, string description, string issueType, string assignee, string key);
    }
}
