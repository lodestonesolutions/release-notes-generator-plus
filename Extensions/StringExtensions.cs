﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Linq;

namespace Pattern.Extensions
{
    public static class StringExtensions
    {
        public static string ReplaceLastOccurrence(this string source, string find, string replace)
        {
            var place = source.LastIndexOf(find, StringComparison.Ordinal);
            var result = source.Remove(place, find.Length).Insert(place, replace);
            return result;
        }

        public static string FormatField(this string s, string fieldFormat)
        {
            //TODO: add data parsing before formatting
            return string.Format(s, fieldFormat);
        }
        public static bool IsValueValid(this string s, string regEx)
        {
            if (string.IsNullOrEmpty(regEx))
            {//no regex provided
                return true;
            }

            var regex = new Regex(regEx);
            var match = regex.Match(s);
            return match.Success;
        }

        public static T ToEnum<T>(this string s)
        {
            try
            {
                var enumVal = (T)Enum.Parse(typeof(T), s);

                return enumVal;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("'{0}' is not a member of the Colors enumeration.", s);
            }

            return default(T) ;
        }

        public static string ToCapital(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            if (s.Length == 1)
            {
                return s.ToUpper();
            }
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static string[] SplitCustom(this string s)
        {
            var splits = s.Split(',');
            return splits;

        }

        public static string[] SplitCustom(this string s, int tokenCount)
        {
            var splits = s.Split(',');
            return splits.Take(tokenCount).ToArray();
        }

        public static string[] SplitCustomIgnoreTrailingComma(this string s)
        {
            var splits = s.Split(',');

            if (splits[splits.Length - 1].Length == 0)
                return splits.Take(splits.Length - 1).ToArray();
            else
                return splits;
        }

        public static string RemoveLastCharacter(this string s)
        {
            if (s.Length > 1)
                return s.Substring(0, s.Length - 1);
            else
                return s;

        }

        public static bool IsValid(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Checks if the string is null or empty
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string src)
        {
            return string.IsNullOrEmpty(src);
        }

        /// <summary>
        /// Checks if the string is null or empty and returns a default value if true
        /// </summary>
        /// <param name="src"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string IsNullOrEmpty(this string src, string defaultValue)
        {
            return string.IsNullOrEmpty(src) ? defaultValue : src;
        }

        /// <summary>
        /// Checks if the string is NOT null or empty
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string src)
        {
            return !string.IsNullOrEmpty(src);
        }

        /// <summary>
        /// If string is null or empty, then use the default value
        /// </summary>
        /// <param name="src"></param>
        /// /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static string WithDefault(this string src, string defaultVal)
        {
            return src.IsNullOrEmpty() ? defaultVal : src;
        }

        /// <summary>
        /// Parses an integer from a string, returns default if parsing fails
        /// </summary>      
        public static int ToInt(this string val, int defaultVal = 0)
        {
            int i;
            return int.TryParse(val, out i) ? i : defaultVal;
        }

        public static string Fmt(this string s, params object[] args)
        {
            return string.Format(s,args);
        }
    }
}
