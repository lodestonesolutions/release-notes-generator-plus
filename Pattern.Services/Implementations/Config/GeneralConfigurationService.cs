﻿using System;
using System.Configuration;
using Castle.Core;
using Pattern.Services.Interfaces;

namespace Pattern.Services.Implementations.Config
{
    [CastleComponent("config.general", typeof(IConfigurationService))]
    public class GeneralConfigurationService : IConfigurationService
    {
        public T GetAppSettingValue<T>(string key, T defaultVal)
        {
            var tp = typeof(T);
            var r = ConfigurationManager.AppSettings[key];

            try
            {
                var result = (T)Convert.ChangeType(r, tp);

                return (result != null) ? result : defaultVal;
            }
            catch (Exception)
            {
                return defaultVal;
            }
        }

        public T GetAppSettingValue<T>(string key)
        {
            return GetAppSettingValue(key, default(T));
        }

        public string GetAppSettingValue(string key)
        {
            return GetAppSettingValue<string>(key, null);
        }

        public string GetAppSettingValue(string key, string defaultVal)
        {
            return GetAppSettingValue<string>(key, defaultVal);
        }
    }
}
