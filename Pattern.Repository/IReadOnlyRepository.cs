﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;

namespace Pattern.Repository
{
    public interface IReadOnlyRepository<TEntity, in TKey> : IDisposable where TEntity : class
    {
        TEntity PrototypeGenericEntity { get; set; }
        string Fields { get; }//either * or fieldA,fieldB,...,FieldX
        string FromSource { get; }
        string SelectFromSource { get; set; }//should be "SELECT [Fields] FROM [FromSource]"
        string SelectFilter { get; set; }//a static where clause.
        IDbConnection Connection { get; set; }
        TEntity Get(TKey id);
        IEnumerable<TEntity> GetAll();       
        IEnumerable<TEntity> GetAll(string where);
        IEnumerable<TEntity> GetAll(string fields, string where);
        IEnumerable<IEnumerable<TEntity>> GetAllYield();
        bool ExecuteCommand(string sSqlQuery);
    }
}