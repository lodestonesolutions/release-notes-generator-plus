﻿using System;
using FluentNHibernate.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pattern.Security.Entities.Entities;
using Pattern.Security.Entities.Interfaces;
using Pattern.Security.UnitTest.Comparers;

namespace Pattern.Security.UnitTest.Tests
{
    [TestClass]
    public class MappingTest
    {
        [TestMethod]
        public void CanMapUser()
        {
            var repo = IoC.Container.Instance.Resolve<ISecurityRepository<User, int>>();

            using (var uow = repo.BeginUnitOfWork())
            {
                new PersistenceSpecification<User>(uow.Session)
                    .CheckProperty(x => x.Username, "")
                    .CheckProperty(x => x.IsActive, false)
                    .CheckProperty(x => x.CreatedOn, DateTime.Now, new DateTimeComparer())
                    .CheckProperty(x => x.CreatedBy, "test")
                    .CheckProperty(x => x.ModifiedOn, DateTime.Now, new DateTimeComparer())
                    .CheckProperty(x => x.ModifiedBy, "test")
                    .VerifyTheMappings();

                uow.Rollback();
            }
        }

        [TestMethod]
        public void CanMapRole()
        {
            var repo = IoC.Container.Instance.Resolve<ISecurityRepository<Role, int>>();

            using (var uow = repo.BeginUnitOfWork())
            {
                new PersistenceSpecification<Role>(uow.Session)
                    .CheckProperty(x => x.RoleName, "")
                    .CheckProperty(x => x.IsActive, false)
                    .CheckProperty(x => x.CreatedOn, DateTime.Now, new DateTimeComparer())
                    .CheckProperty(x => x.CreatedBy, "test")
                    .CheckProperty(x => x.ModifiedOn, DateTime.Now, new DateTimeComparer())
                    .CheckProperty(x => x.ModifiedBy, "test")
                    .VerifyTheMappings();

                uow.Rollback();
            }
        }

        [TestMethod]
        public void CanMapActivity()
        {
            var repo = IoC.Container.Instance.Resolve<ISecurityRepository<Activity, int>>();

            using (var uow = repo.BeginUnitOfWork())
            {
                new PersistenceSpecification<Activity>(uow.Session)
                    .CheckProperty(x => x.ActivityName, "")
                    .CheckProperty(x => x.IsActive, false)
                    .CheckProperty(x => x.CreatedOn, DateTime.Now, new DateTimeComparer())
                    .CheckProperty(x => x.CreatedBy, "test")
                    .CheckProperty(x => x.ModifiedOn, DateTime.Now, new DateTimeComparer())
                    .CheckProperty(x => x.ModifiedBy, "test")
                    .VerifyTheMappings();

                uow.Rollback();
            }
        }
    }
}
