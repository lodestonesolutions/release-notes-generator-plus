﻿
namespace Pattern.Services.Jira.Domain
{
    public class Field
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
