﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Castle.Core.Internal;
using log4net;
using Pattern.Services.Classes;
using Pattern.Services.Interfaces;
using Pattern.Services.Properties;

namespace Pattern.Services.Implementations
{
    //[CastleComponent(typeof(IMailService))]
    public class MailService : IMailService
    {
        private readonly ITextTemplateService _templateService;
        private readonly ILog _logger = LogManager.GetLogger(typeof(MailService));
        private readonly string _signerName;

        public MailService(IConfigurationService configurationService, ITextTemplateService templateService)
        {
            _templateService = templateService;
            _signerName = configurationService.GetAppSettingValue(Settings.Default.SignerName);
        }

        public void SendMail(MailMessage message)
        {
            using (var smtp = new SmtpClient())
            {
                smtp.Send(message);
            }
        }

        public void SendMail(string[] to, string subject, string body, bool isHtml, bool isSigned = false)
        {
            var message = CreateMail(to, subject, body, isHtml, isSigned);
            SendMail(message);
        }

        public void SendWithTemplate(string[] to, string subject, bool isHtml, string templateFilename, IList<TextToken> tokens, bool isSigned = false)
        {
            var template = _templateService.LoadTemplate(templateFilename);
            template.AddTokens(tokens);
            var body = template.Process();
            var message = CreateMail(to, subject, body, isHtml, isSigned);
            SendMail(message);
        }

        public void SendWithTemplate(string[] to, string subject, bool isHtml, TextTemplate template, bool isSigned = false)
        {
            var body = template.Process();
            var message = CreateMail(to, subject, body, isHtml, isSigned);
            SendMail(message);
        }

        public void SignMessage(MailMessage msg)
        {
            try
            {
                var signerCert = GetSignerCert();

                if (signerCert == null)
                    throw new ApplicationException("Could not find a certificate with which to sign the mail.");

                var unicode  = Encoding.Unicode;
                var msgBytes = unicode.GetBytes(msg.Body);

                var signedBytes = SignMsg(msgBytes, signerCert);

                var ms = new MemoryStream(signedBytes);
                var av = new AlternateView(ms, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
                msg.AlternateViews.Add(av);
            }
            catch (Exception ex)
            {
                if (_logger.IsErrorEnabled)
                    _logger.Error(ex);

                throw;
            }       
        }

        private MailMessage CreateMail(IEnumerable<string> to, string subject, string body, bool isHtml, bool isSigned)
        {
            var message = new MailMessage
            {
                Subject = subject,
                IsBodyHtml = isHtml,
                Body = body
            };

            to.ForEach(x => message.To.Add(x));

            if (isSigned) SignMessage(message);

            return message;
        }

        private X509Certificate2 GetSignerCert()
        {
            var storeMy = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
            storeMy.Open(OpenFlags.ReadOnly);

            //Find the signer's certificate.
            var certColl = storeMy.Certificates.Find(X509FindType.FindBySubjectName, _signerName, false);
            _logger.DebugFormat("Found {0} certificates in the {1} store with name {2}", certColl.Count, storeMy.Name, _signerName);

            //Check to see if the certificate is not present.
            if (certColl.Count == 0)
            {
                _logger.ErrorFormat("The certificate '{0}' is not in the '{1}' certificate store. Use an alternate certificate to sign the message.", _signerName, storeMy.Name);
                storeMy.Close();
                return null;
            }

            storeMy.Close();

            return certColl[0];
        }

        private static byte[] SignMsg(Byte[] msg, X509Certificate2 signerCert)
        {
            var contentInfo = new ContentInfo(msg);
            var signedCms = new SignedCms(contentInfo);
            var cmsSigner = new CmsSigner(signerCert);

            //Sign the CMS/PKCS #7 message.
            signedCms.ComputeSignature(cmsSigner);
            
            //  Encode the CMS/PKCS #7 message.
            return signedCms.Encode();
        }
    }
}
