﻿using Pattern.Services.Classes;

namespace Pattern.Services.Interfaces
{
    public interface ITextTemplateService
    {
        TextTemplate LoadTemplate(string templateFilename);
    }
}
