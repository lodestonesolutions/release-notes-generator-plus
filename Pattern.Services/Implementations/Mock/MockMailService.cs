﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Castle.Core.Internal;
using Pattern.Services.Classes;
using Pattern.Services.Interfaces;

namespace Pattern.Services.Implementations.Mock
{
    public class MockMailService: IMailService
    {
        private readonly IList<MailMessage> _messages = new List<MailMessage>();

        public void SendMail(MailMessage message)
        {
            _messages.Add(message);
        }

        public void SendMail(string[] to, string subject, string body, bool isHtml, bool isSigned = false)
        {
            var message = CreateMail(to, subject, body, isHtml, isSigned);
            SendMail(message);
        }

        public void SendWithTemplate(string[] to, string subject, bool isHtml, string templateFilename, IList<TextToken> tokens, bool isSigned = false)
        {
            //var template = _templateService.LoadTemplate(templateFilename);
            //template.AddTokens(tokens);
            //var body = template.Process();
            var message = CreateMail(to, subject, "", isHtml, isSigned);
            SendMail(message);
        }

        public void SendWithTemplate(string[] to, string subject, bool isHtml, TextTemplate template, bool isSigned = false)
        {
            var body = template.Process();
            var message = CreateMail(to, subject, body, isHtml, isSigned);
            SendMail(message);
        }

        public void SignMessage(MailMessage msg)
        {
            throw new NotImplementedException();
        }

        private MailMessage CreateMail(IEnumerable<string> to, string subject, string body, bool isHtml, bool isSigned)
        {
            var message = new MailMessage
            {
                Subject = subject,
                IsBodyHtml = isHtml,
                Body = body
            };

            to.ForEach(x => message.To.Add(x));

            if (isSigned) SignMessage(message);

            return message;
        }
    }
}
