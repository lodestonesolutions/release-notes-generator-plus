﻿using System;

namespace Pattern.Services.Jira.Exceptions
{
    public class JiraException: Exception
    {
        public JiraException()
        {
            
        }

        public JiraException(string message) : base(message)
        {

        }
    }
}
