﻿using System.Web.Mvc;
using log4net;
using Pattern.IoC.Mvc.Interfaces;

namespace Pattern.IoC.Mvc.Base
{
    public abstract class PatternBaseController<T> : Controller, IPatternBaseController
    {
        protected ILog Logger = LogManager.GetLogger(typeof(T));

        
    }
}
