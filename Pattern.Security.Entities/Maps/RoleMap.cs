﻿using FluentNHibernate.Mapping;
using Pattern.Security.Entities.Constants;
using Pattern.Security.Entities.Entities;

namespace Pattern.Security.Entities.Maps
{
    public class RoleMap : ClassMap<Role>
    {
        public RoleMap()
        {
            Table(TableNames.RoleTable);

            Id(x => x.Id, "SID").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.RoleName, "ROLENAME").Not.Nullable();
            Map(x => x.IsActive, "ISACTIVE").Not.Nullable();

            Map(x => x.CreatedOn, "CREATEDON").Not.Nullable();
            Map(x => x.CreatedBy, "CREATEDBY").Not.Nullable();
            Map(x => x.ModifiedOn, "MODIFIEDON").Not.Nullable();
            Map(x => x.ModifiedBy, "MODIFIEDBY").Not.Nullable();

            HasManyToMany(x => x.Activities).Not.LazyLoad().Table("T_ROLE_ACTIVITY").ParentKeyColumn("ROLE_FK").ChildKeyColumn("ACTIVITY_FK");
            HasManyToMany(x => x.Users).Not.LazyLoad().Table("T_USER_ROLE").ParentKeyColumn("ROLE_FK").ChildKeyColumn("USER_FK").Cascade.SaveUpdate();
        }
    }
}
