﻿using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pattern.Services.Interfaces;
using Pattern.Services.Jira.Constants;
using Pattern.Services.Jira.Domain;
using Pattern.Services.Jira.Interfaces;
using Pattern.Services.Jira.Properties;
using RestSharp;
using RestSharp.Authenticators;

namespace Pattern.Services.Jira.Services
{
    public class JiraAccessService : IJiraAccessService
    {
        private readonly string _jiraServiceBaseUrl;
        private readonly string _username;
        private readonly string _pw;

        public JiraAccessService(IConfigurationService configService)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            _jiraServiceBaseUrl = Settings.Default.JiraServiceBaseUrlRest;
            //_jiraServiceAuthUrl = configService.GetAppSettingValue(AppSettingsKeys.JiraServiceAuthUrl);



            _username = Settings.Default.JiraUsername;
            _pw = Settings.Default.JiraPw;
            //_username = "dgt";
            //_pw = "M)Om0o";

            if (string.IsNullOrEmpty(_jiraServiceBaseUrl))
                throw new ArgumentException("Jira Base Service URL cannot be blank or null");
        }

        public Issue GetIssue(string key)
        {
            var client = new RestClient(_jiraServiceBaseUrl)
            {
                CookieContainer = new CookieContainer(),
                Authenticator = new HttpBasicAuthenticator(_username, _pw)
            };

            var request = new RestRequest("issue/{id}", Method.GET);
            request.AddUrlSegment("id", key);
            
            var response = client.Execute(request);

            var issue = JsonConvert.DeserializeObject<RestIssue>(response.Content);

            if (issue == null)
            {
                return new Issue() { HasData = false};
            }

            issue.HasData = issue.Status != null;

            return issue;
        }

        /// <summary>
        /// This function accesses the JIRA RESTful API via GET by recieiving the api route and authenticating with the provided default credentials.
        /// </summary>
        /// <param name="restQueryString">ex in order to get an issue: issue/{key} -> issue/90</param>
        /// <returns></returns>
        public JToken AccessApiAsJToken(string restQueryString)
        {
            var client = new RestClient(_jiraServiceBaseUrl)
            {
                CookieContainer = new CookieContainer(),
                Authenticator = new HttpBasicAuthenticator(_username, _pw)
            };

            var request = new RestRequest(restQueryString, Method.GET);

            var response = client.Execute(request);

            var res = JsonConvert.DeserializeObject<JToken>(response.Content);

            return res;
        }

        public JToken GetIssueAsJToken(string key)
        {
            var issue = AccessApiAsJToken($"issue/{key}");
            return issue;
        }     

        public JToken GetFilterAsJToken(string filterId)
        {
            var filter = AccessApiAsJToken($"filter/{filterId}");
            return filter;
        }

        public Issue CreateIssue(string summary, string description, string issueType, string assignee, string key)
        {
            var client = new RestClient(_jiraServiceBaseUrl)
            {
                CookieContainer = new CookieContainer(),
                Authenticator = new HttpBasicAuthenticator(_username, _pw)
            };

            var newIssue = new Issue
            {
                Description = description,
                Summary = summary,
                IssueType = new IssueType { Id = issueType },
                Project = new Project { Key = key }
            };
            var request = new RestRequest("issue/", Method.POST);
            
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(newIssue);
            //request.AddUrlSegment("id", key);

            var response = client.Execute<Issue>(request);
            var returnedIssue = response.Data;
            newIssue.Key = returnedIssue.Key;
            newIssue.Self = returnedIssue.Self;
            //issue.LoadPropertiesFromFields();

            return newIssue;
        }

        public void UpdateIssue(Issue issue, string key)
        {
            throw new NotImplementedException();
        }

        public void UpdateStatus(string status, string key)
        {
            throw new NotImplementedException();
        }

        public void AddComment(string comment, string key)
        {
            var client = new RestClient(_jiraServiceBaseUrl)
            {
                CookieContainer = new CookieContainer(),
                Authenticator = new HttpBasicAuthenticator(_username, _pw)
            };

            var c = new {body = comment};
            var request = new RestRequest("issue/" + key + "/comment/", Method.POST);

            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(c);
            //request.AddUrlSegment("id", key);

            var response = client.Execute<Issue>(request);
        }
    }
}
