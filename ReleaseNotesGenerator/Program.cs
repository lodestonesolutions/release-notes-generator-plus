﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Windsor;
using Pattern.Entities.Enums;
using Pattern.IoC;

namespace ReleaseNotesGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            InitModules();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        private static void InitModules()
        {
            var wc = new WindsorContainer();
            Container.Instance = wc;

            wc.Install(
                new Pattern.IoC.ModuleInstaller(),
                new Pattern.Services.ModuleInstaller(),
                new Pattern.Services.Jira.ModuleInstaller(),
                new Pattern.Services.Bonobo.ModuleInstaller()
                );
        }
    }
}
