﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pattern.Services.Bonobo.Interfaces;
using RestSharp;
using RestSharp.Authenticators;

namespace Pattern.Services.Bonobo.Services
{
    public class BonoboQueryService : IBonoboQueryService
    {
        public JToken GetCommitsByJiraIdAsJToken(string jiraId)
        {
            var commits = AccessApiAsJToken($"GitQuery/GetJiraCommits?jiraTicketId={jiraId}");

            return commits;
        }

        private JToken AccessApiAsJToken(string restQueryString)
        {
            var client = new RestClient("http://10.2.5.1/gitrepo/")
            {
                CookieContainer = new CookieContainer()
            };

            var request = new RestRequest(restQueryString, Method.GET);
            request.Timeout = 9999999;
            var response = client.Execute(request);

            var res = JsonConvert.DeserializeObject<JToken>(response.Content);

            return res;
        }
    }
}
