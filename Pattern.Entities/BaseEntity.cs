﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace Pattern.Entities
{
    [Serializable]
    public abstract class BaseEntity<T, TKey> where T : BaseEntity<T, TKey>
    {
        private int? _oldHashCode;

        /// <summary>
        /// Get or set the Id of this entity
        /// </summary>
        public virtual TKey Id { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as T;

            if (other == null)
                return false;

            //to handle the case of comparing two new objects
            var otherIsTransient = Equals(other.Id, default(TKey));
            var thisIsTransient = Equals(this.Id, default(TKey));

            if (otherIsTransient && thisIsTransient)
                return ReferenceEquals(other, this);

            return other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            if (_oldHashCode.HasValue)
                return _oldHashCode.Value;

            bool thisIsTransient = Equals(this.Id, default(TKey));

            //When we are transient, we use the base GetHashCode()
            //and remember it, so an instance can't change its hash code.

            if (thisIsTransient)
            {
                _oldHashCode = base.GetHashCode();
                return _oldHashCode.Value;
            }

            return Id.GetHashCode();
        }

        /// <summary>
        /// Throws an exception if the argument is null
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="value"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        protected static U NotNull<U>(U value, string name) where U : class
        {
            if (value == null)
                throw new ArgumentNullException(name);

            return value;
        }

        public static bool operator ==(BaseEntity<T, TKey> x, BaseEntity<T, TKey> y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(BaseEntity<T, TKey> x, BaseEntity<T, TKey> y)
        {
            return !(x == y);
        }
        [IgnoreDataMember]
        public virtual object this[string propertyName]
        {
            get
            {
                try
                {
                    // probably faster without reflection:
                    // like:  return Properties.Settings.Default.PropertyValues[propertyName] 
                    // instead of the following
                    var myType = typeof(T);
                    var myPropInfo = myType.GetProperty(propertyName);
                    return myPropInfo.GetValue(this, null);
                }
                catch (Exception e)
                {
                    return null;
                }
                
            }
            set
            {
                var myType = typeof(T);
                var myPropInfo = myType.GetProperty(propertyName);
                myPropInfo.SetValue(this, value, null);

            }

        }
    }
}
