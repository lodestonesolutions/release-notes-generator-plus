﻿using System;
using System.IO;
using Castle.Core;
using Pattern.Services.Classes;
using Pattern.Services.Interfaces;

namespace Pattern.Services.Implementations
{
    [CastleComponent(typeof(ITextTemplateService))]
    public class TextTemplateService : ITextTemplateService
    {
        public TextTemplate LoadTemplate(string templateFilename)
        {
            if (!File.Exists(templateFilename))
                throw new ArgumentException("File at path " + templateFilename + " does not exist");

            string txt;
            using (var sr = new StreamReader(templateFilename))
            {
                txt = sr.ReadToEnd();
                sr.Close();
            }

            var template = new TextTemplate(txt);

            return template;
        }
    }
}
