﻿using Castle.Windsor;
using Pattern.IoC.ServiceLocator;

namespace Pattern.IoC
{
    public static class Container
    {
        public static IWindsorContainer Instance { get; set; }

        public static IServiceLocator ServiceLocator
        {
            get
            {
                var sl = Instance.Resolve<IServiceLocator>();
                Instance.Release(sl);
                return sl;
            }
        }
    }
}
