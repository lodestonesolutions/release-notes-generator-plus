﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Pattern.Extensions
{
    public static class ObjectExtensions
    {
        public static List<string> GetProperties(this object obj)
        {
            var typeSource = obj.GetType();
            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            return propertyInfo.Where(property => property.CanWrite).Select(property => property.Name).ToList();
        }

        public static Dictionary<string, Type> GetPropertiesType(this object obj)
        {
            var propsAndTypes = new Dictionary<string, Type>();

            var typeSource = obj.GetType();
            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            
            foreach(var p in propertyInfo)
            {
                if (!propsAndTypes.ContainsKey(p.Name))
                {
                    if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        var propType = p.PropertyType.GetGenericArguments()[0];
                        propsAndTypes.Add(p.Name, propType);
                    }
                    else
                    {
                        propsAndTypes.Add(p.Name, p.PropertyType);
                    }
                }
            }

            return propsAndTypes;
        }

        public static object CloneObjectWithReflection(this object objSource)
        {
            //Get the type of source object and create a new instance of that type
            var typeSource = objSource.GetType();
            var objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            //Assign all source property to taget object 's properties
            foreach (var property in propertyInfo.Where(property => property.CanWrite))
            {
                //check whether property type is value type, enum or string type
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String))
                {
                    property.SetValue(objTarget, property.GetValue(objSource, null), null);
                }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                else
                {
                    var objPropertyValue = property.GetValue(objSource);
                    property.SetValue(objTarget, objPropertyValue == null ? null : objPropertyValue.CloneObjectWithReflection(), null);
                }
            }
            return objTarget;
        }

        public static T CopyObjectWithSerializer<T>(this object objSource)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, objSource);
                stream.Position = 0;
                var res = (T)formatter.Deserialize(stream);
                return res;
            }
        }

    }
}
