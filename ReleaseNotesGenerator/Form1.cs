﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core.Internal;

namespace ReleaseNotesGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            rtbLog.AppendText($"Generating new report for JIRA - {tbJira.Text}... \r\n");
            var engine = new ReportEngine();
            engine.Run(tbJira.Text, "MainReleaseDocument.docx");
            rtbLog.AppendText($"Finished generating report for JIRA - {tbJira.Text}. \r\n");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tbFilterId.Text.IsNullOrEmpty())
            {
                rtbTimeSheet.Text += "\r\nPlease insert a time sheet filter Id.";
                return;
            }
            rtbTimeSheet.Text += "\r\nGenerating time sheet...";
            var engine = new TimeSheetEngine();
            engine.Run(tbFilterId.Text);
            rtbTimeSheet.Text += "\r\nTime sheet generation has been completed.";
        }
    }
}
