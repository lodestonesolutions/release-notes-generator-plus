﻿using Pattern.Entities.Enums;

namespace Pattern.Extensions
{
    public static class NHibernateExtensions
    {
        public static string GetSessionContext(this RegistrationLifestyle self)
        {
            switch (self)
            {
                case RegistrationLifestyle.Web:
                    return "web";
                case RegistrationLifestyle.Application:
                    return "call";
                case RegistrationLifestyle.Scoped:
                    return "call";
                default:
                    return "thread_static";
            }
        }
    }
}
