﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using Newtonsoft.Json.Linq;
using OpenXmlPowerTools;
using Pattern.Extensions;
using Pattern.Services.Jira.Interfaces;
using Pattern.IoC;
using Pattern.Services.Bonobo.Interfaces;
using Pattern.Services.Jira.Domain;

namespace ReleaseNotesGenerator
{
    public class ReportEngine
    {
        private readonly IJiraAccessService _jiraService = Pattern.IoC.Container.Instance.Resolve<IJiraAccessService>();
        private readonly IBonoboQueryService _bonoboService = Pattern.IoC.Container.Instance.Resolve<IBonoboQueryService>();

        public void Run(string jiraId, string mainReportTemplateFileName)
        {
            var tickets = GetJiraTicketsSummary(jiraId);

            //get the main reports docx file
            var n = DateTime.Now;
            var tempDi = new DirectoryInfo(
                $"ExampleOutput-{n.Year - 2000:00}-{n.Month:00}-{n.Day:00}-{n.Hour:00}{n.Minute:00}{n.Second:00}");
            tempDi.Create();
            var ds = new List<DocumentSegment>();

            WmlDocument doc1 = new WmlDocument(@"..\..\..\WordTemplates\MainReleaseDocument.docx");
            using (MemoryStream mem = new MemoryStream())
            {
                mem.Write(doc1.DocumentByteArray, 0, doc1.DocumentByteArray.Length);
                using (WordprocessingDocument doc = WordprocessingDocument.Open(mem, true))
                {
                    GenerateTicketSegments(tickets, ds, doc);
                    GenerateReferenceSegment(tickets, ds, doc);
                    GenerateTableSegment(tickets, ds, doc);

                    doc.MainDocumentPart.PutXDocument();
                }
                doc1.DocumentByteArray = mem.ToArray();
            }

            doc1 = TextReplacer.SearchAndReplace(doc1, "{DateTime.Now}", DateTime.Now.Date.ToString("dd/MM/yy"), false);
            doc1 = TextReplacer.SearchAndReplace(doc1, "{Project}", "BEAT", false);
            doc1 = TextReplacer.SearchAndReplace(doc1, "{JiraReleaseTicket}", jiraId, false);

            string outFileName = Path.Combine(tempDi.FullName, $"HW Release Notes - {jiraId} v1.docx");
            var sources = new List<Source>()
            {
                new Source(doc1, true)
            };

            sources.AddRange(ds.Select(s => new Source(s.Document, s.Id)));

            DocumentBuilder.BuildDocument(sources, outFileName);
        }

        private void GenerateTableSegment(List<JiraTicket> tickets, List<DocumentSegment> ds, WordprocessingDocument doc)
        {
            var xe = new XElement(W.t);

            var seg = GenerateTicketIntro(xe, tickets);
            ds.Add(seg);

            XDocument xDoc = doc.MainDocumentPart.GetXDocument();
            XElement tr =
                xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{IntroTable}"));
            tr?.ReplaceWith(xe);
        }

        private static DocumentSegment GenerateTicketIntro(XElement xe, List<JiraTicket> jiraTickets)
        {
            WmlDocument doc1 = new WmlDocument(@"..\..\..\WordTemplates\IntroTableSegment.docx");
            using (MemoryStream mem = new MemoryStream())
            {
                mem.Write(doc1.DocumentByteArray, 0, doc1.DocumentByteArray.Length);
                using (WordprocessingDocument doc = WordprocessingDocument.Open(mem, true))
                {
                    XDocument xDoc = doc.MainDocumentPart.GetXDocument();
                    XElement tbl = xDoc.Root.Element(W.body).Elements(W.tbl).FirstOrDefault();
                    foreach (var jiraTicket in jiraTickets)
                    {
                        var xeTr = new XElement(tbl.Descendants(W.tr).Skip(1).First());

                        var jit = xeTr.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{JiraId}"));
                        var jttt =
                            xeTr.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("JiraTitle"));
                        var jtec =
                            xeTr.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("ExpectedChange"));

                        var jtt = new XElement(W.t) { Value = jiraTicket.Title };
                        var ji = new XElement(W.t) { Value = jiraTicket.JiraKey };
                        var jec = new XElement(W.t) { Value = jiraTicket.ExpectedChange ?? "" };

                        jit?.ReplaceWith(ji);
                        jttt?.ReplaceWith(jtt);
                        jtec?.ReplaceWith(jec);
                        tbl.Add(xeTr);
                    }

                    tbl.Descendants(W.tr).Skip(1).First().Remove();

                    doc.MainDocumentPart.PutXDocument();
                }
                doc1.DocumentByteArray = mem.ToArray();
            }

            var ds = new DocumentSegment
            {
                Document = doc1,
                Id = "TicketsIntro"
            };

            xe.Add(new XElement(PtOpenXml.Insert, new XAttribute("Id", "TicketsIntro")));
            return ds;

        }

        private static void GenerateReferenceSegment(List<JiraTicket> tickets, List<DocumentSegment> ds, WordprocessingDocument doc)
        {
            var xe = new XElement(W.t);

            var seg = GenerateTicketReferences(xe, tickets);
            ds.Add(seg);

            XDocument xDoc = doc.MainDocumentPart.GetXDocument();
            XElement tr =
                xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{DocumentReference}"));
            tr?.ReplaceWith(xe);

        }

        private static DocumentSegment GenerateTicketReferences(XElement xe, List<JiraTicket> jiraTickets)
        {
            WmlDocument doc1 = new WmlDocument(@"..\..\..\WordTemplates\DocumentReferenceSegment.docx");
            using (MemoryStream mem = new MemoryStream())
            {
                mem.Write(doc1.DocumentByteArray, 0, doc1.DocumentByteArray.Length);
                using (WordprocessingDocument doc = WordprocessingDocument.Open(mem, true))
                {
                    XDocument xDoc = doc.MainDocumentPart.GetXDocument();
                    XElement tbl = xDoc.Root.Element(W.body).Elements(W.tbl).FirstOrDefault();
                    foreach (var jiraTicket in jiraTickets)
                    {
                        var xeTr = new XElement(tbl.Descendants(W.tr).Skip(1).First());

                        var jit = xeTr.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{JiraId}"));
                        var jit2 =
                            xeTr.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("JiraIdUrl"));
                        var jttt =
                            xeTr.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("JiraTicketTitle"));
                        var jtt = new XElement(W.t) { Value = jiraTicket.Title };
                        var ji = new XElement(W.t) { Value = jiraTicket.JiraKey };
                        var ji2 = new XElement(W.t) { Value = "https://jira.sicorp.nsw.gove.au/browse/" + jiraTicket.JiraKey };

                        jit?.ReplaceWith(ji);
                        jit2?.ReplaceWith(ji2);
                        jttt?.ReplaceWith(jtt);
                        tbl.Add(xeTr);
                    }

                    tbl.Descendants(W.tr).Skip(1).First().Remove();

                    doc.MainDocumentPart.PutXDocument();
                }
                doc1.DocumentByteArray = mem.ToArray();
            }

            var ds = new DocumentSegment
            {
                Document = doc1,
                Id = "TicketsRefs"
            };

            xe.Add(new XElement(PtOpenXml.Insert, new XAttribute("Id", "TicketsRefs")));
            return ds;

        }


        private static void GenerateTicketSegments(List<JiraTicket> tickets, List<DocumentSegment> ds, WordprocessingDocument doc)
        {
            var ts = new XElement(W.t);
            foreach (var t in tickets)
            {
                var seg = GenerateTicketSegment(ts, t);
                ds.Add(seg);
            }

            XDocument xDoc = doc.MainDocumentPart.GetXDocument();
            XElement TicketSegments =
                xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{TicketSegments}"));
            TicketSegments?.ReplaceWith(ts);
        }

        private static DocumentSegment GenerateTicketSegment(XElement ts, JiraTicket t)
        {
            WmlDocument doc1 = new WmlDocument(@"..\..\..\WordTemplates\TicketsSegment.docx");
            using (MemoryStream mem = new MemoryStream())
            {
                mem.Write(doc1.DocumentByteArray, 0, doc1.DocumentByteArray.Length);
                using (WordprocessingDocument doc = WordprocessingDocument.Open(mem, true))
                {
                    XDocument xDoc = doc.MainDocumentPart.GetXDocument();
                    XElement tDesc = xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{TicketDescription}"));
                    var descLines = t.Description.Trim().Split('\n');
                    var xeDesc = new XElement(W.t);
                    foreach (var line in descLines)
                    {
                        
                        var xe = new XElement(W.t);
                        xe.SetValue(line);
                        var lb = new XElement(W.br);
                        xeDesc.Add(xe);
                        xeDesc.Add(lb);
                    }
                    tDesc?.ReplaceWith(xeDesc);
                    //tDesc?.SetValue(t.Description.Replace("\r\n", "</w:t><w:t>"));

                    XElement tTitle = xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("TicketTitle"));
                    //tTitle?.ReplaceWith(new XDocument(W.t, t.JiraKey + ": " + t.Title));
                    tTitle?.SetValue(t.JiraKey + ": " + t.Title);

                    XElement tFiles = xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{FilesAffected}"));
                    var filesLines = t.FilesAffected?.Trim().Split('\n') ?? new string[0];
                    var xeFile = new XElement(W.t);
                    foreach (var line in filesLines)
                    {
                        if (string.IsNullOrWhiteSpace(line.Replace("\r", "").Replace("\n", "")))
                        {
                            continue;
                        }
                        var xe = new XElement(W.t);
                        xe.SetValue(line);
                        var lb = new XElement(W.br);
                        xeFile.Add(xe);
                        xeFile.Add(lb);
                    }
                    tFiles?.ReplaceWith(xeFile);
                    //tFiles?.SetValue(t.FilesAffected);

                    XElement tCode = xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{CodeChanges}"));
                    var codeLines = t.CodeSamples?.Split('\n') ?? new string[0];
                    var xeCode = new XElement(W.t);
                    foreach (var line in codeLines)
                    {
                        var xe = new XElement(W.t);
                        xe.SetValue(line);
                        var lb = new XElement(W.br);
                        xeCode.Add(xe);
                        xeCode.Add(lb);
                    }
                    tCode?.ReplaceWith(xeCode);

                    XElement tTestRes = xDoc.Root.Descendants().Elements(W.t).FirstOrDefault(x => x.Value.Contains("{TestResults}"));
                    tTestRes?.SetValue(t.Attachments.Aggregate("", (current, s) => current + (s + "\r\n")));

                    doc.MainDocumentPart.PutXDocument();
                }
                doc1.DocumentByteArray = mem.ToArray();
            }

            var ds = new DocumentSegment
            {
                Document = doc1,
                Id = "TicketSegments" + t.JiraKey
            };

            ts.Add(new XElement(PtOpenXml.Insert, new XAttribute("Id", "TicketSegments" + t.JiraKey)));
            return ds;
        }

        private List<JiraTicket> GetJiraTicketsSummary(string jiraId)
        {//Get jiraticket by id
            var ticket = _jiraService.GetIssueAsJToken(jiraId);

            //get linked jira tickets to above ticket
            var issueFields = ticket["fields"];
            var issueLinks = issueFields["issuelinks"];

            return issueLinks.Any() ? issueLinks.Select(GetLinkedIssue).ToList() : new List<JiraTicket> {GetLinkedIssue(ticket)};
        }

        private JiraTicket GetLinkedIssue(JToken issueLink)
        {
            var t = new JiraTicket();
            var oi = issueLink["outwardIssue"] ?? issueLink["inwardIssue"];
            //get jira key
            t.JiraKey = oi?["key"].ToString() ?? issueLink["key"].ToString();

            var ti = _jiraService.GetIssueAsJToken(t.JiraKey);
            var bi = _bonoboService.GetCommitsByJiraIdAsJToken(t.JiraKey);

            //get ticket title
            t.Title = ti["fields"]["summary"].ToString();
            //get ticket description
            t.Description = ti["fields"]["description"].ToString();

            //get ticket attachment
            foreach (var a in ti["fields"]["attachment"])
            {
                t.Attachments.Add(a["content"].ToString());
            }

            t.ExpectedChange = ti["fields"]["customfield_11180"]?.ToString();//expected change custom field;

            //find ticket code in comments
            if (bi.First != null)
            {
                foreach (var c in bi[0]["Changes"])
                {
                    t.CodeSamples += c["Patch"] + "\r\n";
                    t.FilesAffected += c["Path"] + "\r\n";
                }
            }

            return t;
        }
    }
}
