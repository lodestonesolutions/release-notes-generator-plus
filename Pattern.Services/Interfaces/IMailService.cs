﻿using System.Collections.Generic;
using System.Net.Mail;
using Pattern.Services.Classes;

namespace Pattern.Services.Interfaces
{
    public interface IMailService
    {
        /// <summary>
        /// Send a mail message
        /// </summary>
        /// <param name="message"></param>
        void SendMail(MailMessage message);

        /// <summary>
        /// Send a mail message
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="isSigned"></param>
        void SendMail(string[] to, string subject, string body, bool isHtml, bool isSigned = false);

        /// <summary>
        /// Send a mail message with a text template
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="isHtml"></param>
        /// <param name="templateFilename"></param>
        /// <param name="tokens"></param>
        /// <param name="isSigned"></param>
        void SendWithTemplate(string[] to, string subject, bool isHtml, string templateFilename, IList<TextToken> tokens, bool isSigned = false);

        /// <summary>
        /// Send a mail message with a text template
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="isHtml"></param>
        /// <param name="template"></param>
        /// <param name="isSigned"></param>
        void SendWithTemplate(string[] to, string subject, bool isHtml, TextTemplate template, bool isSigned = false);

        void SignMessage(MailMessage msg);
    }
}
