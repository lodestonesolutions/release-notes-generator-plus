﻿namespace Pattern.Services.Jira.Domain
{
    public class Status : AddressableNamedEntity
    {
        public Status()
        {
            
        }

        public Status(string name, string self):this()
        {
            Name = name;
            Self = self;
        }
    }
}
