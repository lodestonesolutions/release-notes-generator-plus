﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Pattern.Repository
{
    public interface IRepository<TEntity, in TKey> : IDisposable where TEntity : class
    {
        TEntity Get(TKey id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<IEnumerable<TEntity>> GetAllYield();
        TEntity Create(TEntity entity);
        void Update(TEntity entity);
        void Save(TEntity entity);
        void Delete(TEntity entity);
    }
}