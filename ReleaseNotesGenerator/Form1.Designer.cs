﻿namespace ReleaseNotesGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.tbJira = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabReleaseNotes = new System.Windows.Forms.TabPage();
            this.tabTimeSheet = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.rtbTimeSheet = new System.Windows.Forms.RichTextBox();
            this.tbFilterId = new System.Windows.Forms.TextBox();
            this.lblFilter = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabReleaseNotes.SuspendLayout();
            this.tabTimeSheet.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(170, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbJira
            // 
            this.tbJira.Location = new System.Drawing.Point(64, 20);
            this.tbJira.Name = "tbJira";
            this.tbJira.Size = new System.Drawing.Size(100, 22);
            this.tbJira.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Jira Id";
            // 
            // rtbLog
            // 
            this.rtbLog.Location = new System.Drawing.Point(6, 49);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.Size = new System.Drawing.Size(594, 218);
            this.rtbLog.TabIndex = 3;
            this.rtbLog.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabReleaseNotes);
            this.tabControl1.Controls.Add(this.tabTimeSheet);
            this.tabControl1.Location = new System.Drawing.Point(12, 21);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(778, 484);
            this.tabControl1.TabIndex = 4;
            // 
            // tabReleaseNotes
            // 
            this.tabReleaseNotes.Controls.Add(this.rtbLog);
            this.tabReleaseNotes.Controls.Add(this.button1);
            this.tabReleaseNotes.Controls.Add(this.label1);
            this.tabReleaseNotes.Controls.Add(this.tbJira);
            this.tabReleaseNotes.Location = new System.Drawing.Point(4, 25);
            this.tabReleaseNotes.Name = "tabReleaseNotes";
            this.tabReleaseNotes.Padding = new System.Windows.Forms.Padding(3);
            this.tabReleaseNotes.Size = new System.Drawing.Size(770, 455);
            this.tabReleaseNotes.TabIndex = 0;
            this.tabReleaseNotes.Text = "Release Notes";
            this.tabReleaseNotes.UseVisualStyleBackColor = true;
            // 
            // tabTimeSheet
            // 
            this.tabTimeSheet.Controls.Add(this.lblFilter);
            this.tabTimeSheet.Controls.Add(this.tbFilterId);
            this.tabTimeSheet.Controls.Add(this.rtbTimeSheet);
            this.tabTimeSheet.Controls.Add(this.button2);
            this.tabTimeSheet.Location = new System.Drawing.Point(4, 25);
            this.tabTimeSheet.Name = "tabTimeSheet";
            this.tabTimeSheet.Padding = new System.Windows.Forms.Padding(3);
            this.tabTimeSheet.Size = new System.Drawing.Size(770, 455);
            this.tabTimeSheet.TabIndex = 1;
            this.tabTimeSheet.Text = "Time Sheet";
            this.tabTimeSheet.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(259, 299);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // rtbTimeSheet
            // 
            this.rtbTimeSheet.Location = new System.Drawing.Point(22, 75);
            this.rtbTimeSheet.Name = "rtbTimeSheet";
            this.rtbTimeSheet.Size = new System.Drawing.Size(594, 218);
            this.rtbTimeSheet.TabIndex = 4;
            this.rtbTimeSheet.Text = "";
            // 
            // tbFilterId
            // 
            this.tbFilterId.Location = new System.Drawing.Point(161, 47);
            this.tbFilterId.Name = "tbFilterId";
            this.tbFilterId.Size = new System.Drawing.Size(130, 22);
            this.tbFilterId.TabIndex = 5;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.Location = new System.Drawing.Point(22, 47);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(130, 17);
            this.lblFilter.TabIndex = 6;
            this.lblFilter.Text = "TimeSheet Filter Id:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 530);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabReleaseNotes.ResumeLayout(false);
            this.tabReleaseNotes.PerformLayout();
            this.tabTimeSheet.ResumeLayout(false);
            this.tabTimeSheet.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbJira;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabReleaseNotes;
        private System.Windows.Forms.TabPage tabTimeSheet;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox rtbTimeSheet;
        private System.Windows.Forms.Label lblFilter;
        private System.Windows.Forms.TextBox tbFilterId;
    }
}

