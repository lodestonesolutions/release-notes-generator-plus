﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pattern.Hash.CityHash;

namespace Pattern.Entities
{
    [Serializable]
    public class BaseGenericEntity : IGenericEntity
    {
        public Dictionary<string, Tuple<object, Type>> PropertiesDictionary { get; set; }
        public List<string> PrimaryKeyPropertyComponents { get; set; }
        public string EntitySource { get; set; }
        public BaseGenericEntity()
        {
            PropertiesDictionary = new Dictionary<string, Tuple<object, Type>>();
            PrimaryKeyPropertyComponents = new List<string>();
        }

        public ulong GetId()
        {
            if (PrimaryKeyPropertyComponents.Count == 0)
            {
                ulong hash = 0;
                if (TryGetProperty("Id") != null)
                {
                    hash = TryGetProperty("Id").ToString().GetCityHash64();
                }
                else if (TryGetProperty("id") != null)
                {
                    hash = TryGetProperty("id").ToString().GetCityHash64();
                }
                else if (TryGetProperty("ID") != null)
                {
                    hash = TryGetProperty("ID").ToString().GetCityHash64();
                }
                else if (TryGetProperty("SID") != null)
                {
                    hash = TryGetProperty("SID").ToString().GetCityHash64();
                }
                else if (TryGetProperty("sid") != null)
                {
                    hash = TryGetProperty("sid").ToString().GetCityHash64();
                }
                else if (TryGetProperty("Sid") != null)
                {
                    hash = TryGetProperty("Sid").ToString().GetCityHash64();
                }
                return hash;
            }

            var sb = new StringBuilder();

            foreach (var p in PrimaryKeyPropertyComponents.Select(TryGetProperty))
            {
                if (p == null)
                {
                    throw new Exception("Primary key property component was not found.");
                }
                sb.Append(p);
            }

            return sb.ToString().GetCityHash64();
        }
        public string GetPk()
        {
            if (PrimaryKeyPropertyComponents.Count == 0)
            {
                string res = "";
                if (TryGetProperty("Id") != null)
                {
                    res = TryGetProperty("Id").ToString();
                }
                else if (TryGetProperty("id") != null)
                {
                    res = TryGetProperty("id").ToString();
                }
                else if (TryGetProperty("ID") != null)
                {
                    res = TryGetProperty("ID").ToString();
                }
                else if (TryGetProperty("SID") != null)
                {
                    res = TryGetProperty("SID").ToString();
                }
                else if (TryGetProperty("sid") != null)
                {
                    res = TryGetProperty("sid").ToString();
                }
                else if (TryGetProperty("Sid") != null)
                {
                    res = TryGetProperty("Sid").ToString();
                }
                return res;
            }

            var sb = new StringBuilder();

            foreach (var p in PrimaryKeyPropertyComponents.Select(TryGetProperty))
            {
                if (p == null)
                {
                    throw new Exception("Primary key property component was not found.");
                }
                sb.Append(p);
            }

            return sb.ToString();
        }
        public virtual void RemoveProperty(string key)
        {
            if (PropertiesDictionary.ContainsKey(key))
            {
                PropertiesDictionary.Remove(key);
            }
        }

        public virtual void SetProperty(string key, object value, Type type = null)
        {
            if (!PropertiesDictionary.ContainsKey(key))
            {
                var tuple = Tuple.Create(value, type);
                PropertiesDictionary.Add(key, tuple);
            }
            else
            {
                var tuple = Tuple.Create(value, type ?? TryGetPropertyType(key));
                PropertiesDictionary[key] = tuple;
            }

        }

        public virtual object TryGetProperty(string key)
        {
            Tuple<object, Type> t = null;
            PropertiesDictionary.TryGetValue(key, out t);

            return (t == null) ? null : t.Item1;
        }

        public virtual bool IsEqual(IGenericEntity obj)
        {
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            //check if all props are present and equal.
            return obj.GetHashCode() == GetHashCode();
        }

        public Type TryGetPropertyType(string key)
        {
            Tuple<object, Type> t = null;
            PropertiesDictionary.TryGetValue(key, out t);

            return (t == null) ? null : t.Item2;
        }

        public ulong GetHashCode()
        {
            return PropertiesDictionary.Aggregate<KeyValuePair<string, Tuple<object, Type>>, ulong>(0,
                (current, pair) => current + (pair.Value.Item1 != null ? pair.Value.Item1.ToString().GetCityHash64() : "".GetCityHash64())
            );
        }
        public ulong GetHashCode(List<string> keysOrder)
        {
            return keysOrder.Aggregate<string, ulong>(0, 
                (current1, key) => current1 + (PropertiesDictionary[key].Item1 != null ? PropertiesDictionary[key].Item1.ToString().GetCityHash64() : "".GetCityHash64())
            );
        }

    }
}
