﻿namespace Pattern.Entities.Enums
{
    public enum RegistrationLifestyle
    {
        Web = 1,
        Application = 2,
        WebApi = 3,
        Scoped = 4
    }
}
