﻿using System;
using NHibernate;

namespace Pattern.Repository.NHibernate.Common.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Unique Id of Unit of Work
        /// </summary>
        Guid Id { get; }
        /// <summary>
        /// The current session
        /// </summary>
        ISession Session { get; }
        /// <summary>
        /// The current transaction
        /// </summary>
        ITransaction Transaction { get; }
        /// <summary>
        /// Commit the Unit of Work
        /// </summary>
        void Commit();
        /// <summary>
        /// Rollback the Unit of Work
        /// </summary>
        void Rollback();
    }
}
